import React, {
  FC,
  HTMLAttributes, useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useAtom } from '@reatom/react';
import { emailStore } from './store';
import { authAtom, authUserAtom } from './store/modules/Auth/atoms';
import { Button, Card, message } from 'antd';
import { loadingAtom } from './store/ui/RootTemplate/atoms';
import { Header } from './components/Header';
import { SocketConnection } from './components/SocketConnection';
import { Container } from './components/Container';
import hexToRgba from 'hex-to-rgba';
import { orange } from '@ant-design/colors';
import { RouteType } from './routes';
import { activeWorkspaceAtom, organizationAtom } from './store/modules/Organization/atoms';
import { userOrganizations } from './store/modules/User/atoms';
import { Preloader } from './components/Preloader';
import { Footer } from './components/Footer';

type TemplateType = HTMLAttributes<HTMLDivElement> & { routesList: ReadonlyArray<RouteType> };

export const Template: FC<TemplateType> = (props: TemplateType) => {

  const [loading, { set: setLoading }] = useAtom(loadingAtom);
  const [showLoader, setShowLoader] = useState(loading);
  const [token, { set: setToken }] = useAtom(authAtom);
  const [user, { get: getUser }] = useAtom(authUserAtom);
  const [activeWorkspace, { set: setActiveWorkspace }] = useAtom(activeWorkspaceAtom);
  const socket = useContext(SocketConnection);
  const [organizations, { get: getOrganizations }] = useAtom(userOrganizations);
  const [organization, { get: getOrganization }] = useAtom(organizationAtom);

  const router = useHistory();
  const { pathname } = useLocation();

  const noAuthPaths = (props.routesList.filter(n => n.noAuth).map(m => m.path));
  const fullHeightLayoutPaths = (props.routesList.filter(n => n.fullHeight).map(m => m.path));

  const chooseDefaultWorkspace = useCallback(() => {
    if (localStorage) {
      if (organizations.data?.length) {
        localStorage.setItem('workspaceId', `${organizations.data[0]._id};${token}`);
        setActiveWorkspace(organizations.data[0]._id)
      } else {
        localStorage.removeItem('workspaceId');
      }
    }
  }, [organizations, setActiveWorkspace, token]);

  useEffect(() => {
    if(localStorage?.getItem('authToken')) {
      // @ts-ignore
      setToken(localStorage.getItem('authToken'));
    } else if (!(noAuthPaths.includes(pathname) || noAuthPaths.includes(`${pathname}/`))){
      router.push('/auth');
    }
  }, [getUser, noAuthPaths, pathname, router, setToken]);

  useEffect(() => {
    if (token && localStorage?.getItem('workspaceId')) {
      const workspaceIdLocal = localStorage.getItem('workspaceId')?.split(';');
      if (workspaceIdLocal?.length === 2) {
        if (workspaceIdLocal?.[1] === token) {
          // @ts-ignore
          setActiveWorkspace(workspaceIdLocal?.[0]);
        } else {
          chooseDefaultWorkspace();
        }
      } else {
        chooseDefaultWorkspace();
      }
    } else if (token) {
      chooseDefaultWorkspace();
    }
  }, [chooseDefaultWorkspace, setActiveWorkspace, token]);

  useEffect(() => {
    if (token) {
      getUser();
    }
  }, [getUser, token]);

  useEffect(() => {
    if (token && !organizations.loaded) {
      getOrganizations();
    }
  }, [getOrganizations, organizations.loaded, token]);

  useEffect(() => {
    if (token && activeWorkspace && !organization.loaded) {
      getOrganization(activeWorkspace);
    }
  }, [activeWorkspace, getOrganization, organization.loaded, token]);

  useEffect(() => {
    if (token) {
      if (user.data._id) {
        socket?.emit('auth', { token: localStorage.getItem('authToken'), userId: user.data._id });
      }
      if (user.error) {
        router.push('/auth/logout');
      }
      setLoading(user.loading);
    }
  }, [router, setLoading, socket, token, user]);

  useEffect(() => {
    setTimeout(() => {
      setShowLoader(loading);
    }, 150);
  }, [loading]);

  const resendConfirmation = () => {
    if (token) {
      emailStore.requests.Requests.resend()
        .then(async () => {
          await message.success(`Confirmation code was sent to ${user.data.email}`);
        })
        .catch(async (err) => {
          if (err.response?.data?.message) {
            await message.warn("You have to wait 1 minute");
          } else {
            await message.error("Something went wrong. Try again later.");
          }
        });
    }
  };

  return (
    <div style={{
      ...(fullHeightLayoutPaths.includes(pathname) || fullHeightLayoutPaths.includes(`${pathname}/`) ? { height: '100vh', display: 'flex', flexDirection: 'column' } : {})
    }}>
      <Header />

      {user?.data?.confirmed === false ? (
          <Container size={'100%'} className={'my-3'}>
              <Card title={"Confirm your email"} style={{ borderLeft: `10px solid ${hexToRgba(orange.primary || 'orange', 0.5)}` }}>
                We sent a confirmation code to {user.data.email}.
                <Button type={"link"} onClick={resendConfirmation} className={'p-0 ml-2'}>Resend confirmation code</Button>
              </Card>
          </Container>
      ) : null}

      {props.children}

      <Footer fullHeight={fullHeightLayoutPaths.includes(pathname) || fullHeightLayoutPaths.includes(`${pathname}/`)} />

      {(loading ? loading : showLoader) ? (
        <Preloader style={{
          opacity: (loading ? showLoader : loading) ? 1 : 0,
        }} />
      ) : null}
    </div>
  );
};
