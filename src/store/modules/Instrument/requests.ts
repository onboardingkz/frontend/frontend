import { backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup('instrument');
const prefixAccessRequest = withApiRouteGroup('accessRequest');

export const Requests = {
  create: (data: any) => backendWithAuth().post(`${prefix}`, data),
  get: (id: string) => backendWithAuth().get(`${prefix}/${id}`),
  update: (id: string, data: any) => backendWithAuth().put(`${prefix}/${id}`, data),
  delete: (id: string) => backendWithAuth().delete(`${prefix}/${id}`),
  sendAccessRequest: (id: string) => backendWithAuth().post(`${prefixAccessRequest}`, { instrumentId: id }),
  acceptAccessRequest: (id: string) => backendWithAuth().post(`${prefixAccessRequest}/${id}`),
  declineAccessRequest: (id: string) => backendWithAuth().delete(`${prefixAccessRequest}/${id}`),
}
