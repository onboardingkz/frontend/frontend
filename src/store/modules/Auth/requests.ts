import { backend, backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup('auth');

export const Requests = {
  register: (data: any) => backend.post(`${prefix}/register`, data),
  login: (data: any) => backend.post(`${prefix}/login`, data),
  logout: (data: any) => backendWithAuth().post(`${prefix}/logout`, data),
  user: () => backendWithAuth().get(`${prefix}/user`),
  update: (data: any) => backendWithAuth(null).put(`${prefix}/user/update`, data)
}
