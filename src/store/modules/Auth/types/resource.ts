export type Resource = {
  readonly id?: string;
  readonly firstName?: string;
  readonly secondName?: string;
  readonly username?: string;
}
