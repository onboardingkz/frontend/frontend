import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

export const authUserAtom = createAtom(
  { get: () => null, clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule }, state: any = {
    loading: false,
    data: {},
    error: null
  }) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }));
    });

    onAction('get', () => {
      schedule((dispatch) => {
        dispatch(create('_mutate', { ...state, loading: true, error: null }));
        Requests.user()
          .then(response => {
            dispatch(create('_mutate', { loading: false, data: response.data, error: null }));
          })
          .catch(async (error) => {
            dispatch(create('_mutate', { loading: false, data: {}, error }));
          });
      });
    });

    return state;
  });
