import { createAtom } from '@reatom/core';

export const authAtom = createAtom(
  { clear: () => {}, _mutate: (newState: any) => newState, set: (token: string) => token },
  ({ onAction, create, schedule }, state: any = null) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('set', (token) => {
      state = token;
    });

    return state;
  });
