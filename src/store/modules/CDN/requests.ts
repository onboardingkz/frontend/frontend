import { cdn } from '../../../core/api';

const getCategory = (category: string | null = null) => category ? `?category=${category}` : '';

export const Requests = {
  upload: (data: any, category: string | null = null) => cdn.post(`/upload${getCategory(category)}`, data),
  delete: (file: string) => cdn.delete(`/remove?file=${file}`),
}
