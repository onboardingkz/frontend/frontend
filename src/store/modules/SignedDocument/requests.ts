import { backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup('signedDocument');

export const Requests = {
  create: (data: any) => backendWithAuth().post(`${prefix}`, data),
  delete: (id: string) => backendWithAuth().delete(`${prefix}/${id}`),
}
