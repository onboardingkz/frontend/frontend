import { backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup(`user`);
const prefixUsers = withApiRouteGroup(`users`);
const prefixNotifications = withApiRouteGroup(`notifications`);

export const Requests = {
  getOrganizations: () => backendWithAuth().get(`${prefix}/organizations`),
  getOwnedOrganizations: () => backendWithAuth().get(`${prefix}/organizations/owned`),
  notificationsAll: () => backendWithAuth().get(`${prefixNotifications}/all`),
  notificationsOpened: () => backendWithAuth().post(`${prefixNotifications}/opened`),
  search: (data: any) => backendWithAuth().post(`${prefixUsers}/search`, data),
  get: (id: string) => backendWithAuth().get(`${prefix}/${id}`),

}
