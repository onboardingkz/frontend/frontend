export type Resource = {
  readonly id?: string;
  readonly name?: string;
  readonly lastName?: string;
  readonly email?: string;
  readonly phone?: string;
}
