import { backend, backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup('email');

export const Requests = {
  resend: () => backendWithAuth().post(`${prefix}/resend`),
  confirm: (token: string) => backend.post(`${prefix}/confirm`, { token })
}
