import { backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup('document');

export const Requests = {
  create: (data: any) => backendWithAuth().post(`${prefix}`, data),
  update: (id: string, data: any) => backendWithAuth().put(`${prefix}/${id}`, data),
  delete: (id: string) => backendWithAuth().delete(`${prefix}/${id}`),
}
