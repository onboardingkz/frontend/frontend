import { backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup('department');

export const Requests = {
  create: (data: any) => backendWithAuth().post(`${prefix}`, data),
  get: (id: string) => backendWithAuth().get(`${prefix}/${id}`),
  update: (id: string, data: any) => backendWithAuth().put(`${prefix}/${id}`, data),
  delete: (id: string) => backendWithAuth().delete(`${prefix}/${id}`),
}
