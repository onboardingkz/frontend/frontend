import { backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup('organization');

export const Requests = {
  create: (data: any) => backendWithAuth().post(`${prefix}`, data),
  get: (id: string) => backendWithAuth().get(`${prefix}/${id}`),
  update: (id: string, data: any) => backendWithAuth().put(`${prefix}/${id}`, data),
  delete: (id: string) => backendWithAuth().delete(`${prefix}/${id}`),
  getInstruments: (id: string) => backendWithAuth().get(`${prefix}/${id}/instruments`),
  getRules: (id: string) => backendWithAuth().get(`${prefix}/${id}/rules`),
  getDocuments: (id: string) => backendWithAuth().get(`${prefix}/${id}/documents`),
  getSignedDocuments: (id: string) => backendWithAuth().get(`${prefix}/${id}/signedDocuments`),
  getDepartments: (id: string) => backendWithAuth().get(`${prefix}/${id}/departments`),

  getMembers: (id: string) => backendWithAuth().get(`${prefix}/${id}/members`),
  deleteMember: (id: string, data: any) => backendWithAuth().delete(`${prefix}/${id}/members`, { data }),
  addMember: (id: string, data: any) => backendWithAuth().post(`${prefix}/${id}/members`, data),
}
