import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

const defaultState = {
  error: null,
  data: [],
  loading: false
}

export const organizationInstrumentsAtom = createAtom(
  { get: (id: string) => id, clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule }, state: any = defaultState) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', defaultState));
      }))
    });

    onAction('get', (id) => {
      schedule((dispatch) => {
        dispatch(create('_mutate', { ...defaultState, loading: true }));
        Requests.getInstruments(id)
          .then(response => {
            dispatch(create('_mutate', { ...state, data: response.data, loading: false, error: null }));
          })
          .catch(error => {
            dispatch(create('_mutate', { ...state, data: [], loading: false, error }));
          });
      });
    });

    return state;
  });
