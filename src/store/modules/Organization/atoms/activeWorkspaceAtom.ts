import { createAtom } from '@reatom/core';

export const activeWorkspaceAtom = createAtom(
  { clear: () => {}, _mutate: (newState: any) => newState, set: (id: string) => id },
  ({ onAction, create, schedule }, state: any = null) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('set', (id) => {
      state = id;
    });

    return state;
  });
