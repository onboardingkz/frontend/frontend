import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

const defaultState = {
  error: null,
  data: {},
  loading: false,
  loaded: false
}

export const organizationAtom = createAtom(
  { get: (id: string) => id, clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule }, state: any = defaultState) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', defaultState));
      }))
    });

    onAction('get', (id) => {
      schedule((dispatch) => {
        dispatch(create('_mutate', { ...defaultState, loading: true }));
        Requests.get(id)
          .then(response => {
            dispatch(create('_mutate', { ...defaultState, data: response.data, loaded: true }));
          })
          .catch(error => {
            dispatch(create('_mutate', { ...defaultState, error }));
          });
      });
    });

    return state;
  });
