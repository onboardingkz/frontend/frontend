import { createAtom } from '@reatom/core';

export const loadingAtom = createAtom(
  { set: (newState: boolean) => newState },
  ({ onAction }, state = false) => {

    onAction('set', (newState) => {
      state = newState;
    });

    return state;
  });
