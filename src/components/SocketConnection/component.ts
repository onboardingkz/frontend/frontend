import { createContext } from 'react';
import { Socket } from 'socket.io-client';

export const SocketConnection = createContext<Socket | null>(null);
