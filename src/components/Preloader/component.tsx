import React, { FC } from 'react';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { Props } from './props';

export const Preloader: FC<Props> = ({ style, ...rest }: Props) => {
  return (
    <div
      style={{
        color: '#fff',
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'rgb(18, 76, 90, .85)',
        backdropFilter: 'blur(6px)',
        zIndex: 9999999,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        transition: 'opacity 0.15s',
        ...style,
      }}
      {...rest}
    >
      <Spin indicator={
        <LoadingOutlined
          style={{
            fontSize: 50,
            color: '#fff'
          }}
        />
      } />
    </div>
  );
}
