import React, { Component, ErrorInfo } from "react";
import { Props } from './props';
import { Container } from '../Container';
import { Tooltip, Typography } from 'antd';

import dinosaur from '../../assets/images/dinosaur.png';

interface State {
  hasError: boolean;
}

export class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(_: Error): State {
    // Обновить состояние с тем, чтобы следующий рендер показал запасной UI.
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // Можно также сохранить информацию об ошибке в соответствующую службу журнала ошибок
    console.error("Uncaught error:", error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // Можно отрендерить запасной UI произвольного вида
      return (
        <div style={{ paddingTop: 300 }}>
          <Container size={500}>
            <h1 className={'text-center'} style={{
              fontSize: 70,
              margin: 0
            }}>500</h1>
            <Typography.Title className={'text-center'} level={5}>Oops... Something went wrong</Typography.Title>
            <div className="text-center">
              <Tooltip title={'This is just a meme image. The game doesn\'t work'}>
                <img src={dinosaur} alt={'error 500 onboarding'} />
              </Tooltip>
            </div>
          </Container>
        </div>
      );
    }

    return <>{this.props.children}</>; 
  }
}


