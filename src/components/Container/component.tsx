import styled from '@emotion/styled';
import { Props } from './props';
import { css } from '@emotion/react';

export const Container = styled.div<Props>`
  ${({ size }) => css`
    position: relative;
    max-width: ${typeof size === "string" ? size : `${size || 1200}px`};
    padding: 0 20px;
    margin: auto;
    width: 100%;
  `}
`;
