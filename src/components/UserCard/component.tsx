import React, { FC } from 'react';
import { Avatar } from 'antd';
import { getAvatar } from '../../core/utils';
import { grey } from '@ant-design/colors';
import { Props } from './props';
import classNames from 'classnames';

export const UserCard: FC<Props> = ({ item, className, ...rest }: Props) => {
  return (
    <div className={classNames('d-flex align-items-center py-2', className)} {...rest}>
      <div>
        <Avatar style={{ backgroundColor: 'grey' }} size={'default'} src={getAvatar(item?.avatar)} />
      </div>
      <div className={'ml-3'}>
        <span className={'d-block'} style={{ fontSize: 16 }}>{item?.name || `No name`}</span>
        <span className={'d-inline-block'} style={{ color: grey.primary, fontSize: 13 }}>{item?.email || 'email'}</span>
      </div>
    </div>
  );
}
