import { FC } from 'react';
import { Divider, Typography } from 'antd';
import { Container } from '../Container';
import { grey } from '@ant-design/colors';
import { NavigationItem } from '../Header/libs/NavigationItem';
import { Props } from './props';

export const Footer: FC<Props> = ({ fullHeight }: Props) => {
  return fullHeight ? (
    <div className={'mt-4'}>
      <Container size={'100%'}>
        <Divider className={'m-0'} />
        <div className={'py-3 d-flex align-items-center justify-content-between'}>
          <div>
            <Typography.Title level={4} className={'m-0'}>HoneyComb.kz</Typography.Title>
            <Typography.Paragraph className={'m-0 pt-1'} style={{ color: grey.primary, fontSize: 12 }}>Where your work starts</Typography.Paragraph>
          </div>
          <div>
            <NavigationItem>Blog</NavigationItem>
            <NavigationItem>Status</NavigationItem>
            <NavigationItem>Contacts</NavigationItem>
          </div>
        </div>
      </Container>
    </div>
  ) : null;
}
