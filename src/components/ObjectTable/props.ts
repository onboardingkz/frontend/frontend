import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  item: Record<any, any>;
  columns: Record<any, any>;
}
