import { FC } from 'react';
import { Props } from './props';
import { blue, grey } from '@ant-design/colors';
import hexToRgba from 'hex-to-rgba';

export const ObjectTable: FC<Props> = ({ item, columns, style, ...rest }: Props) => {
  const keys = Object.keys(columns);
  return (
    <div style={{
      border: `1px solid ${hexToRgba(grey.primary || 'grey', 0.3)}`,
      borderRadius: 10,
      ...style
    }} {...rest}>
      {keys.map((n, i) => {
        return (
          <div key={i} css={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            borderBottom: i !== keys.length - 1 ? `1px solid ${hexToRgba(grey.primary || 'grey', 0.3)}` : 'none',
            padding: '10px 20px',
            fontSize: 14,
            fontWeight: 500,
            transition: 'background-color 0.3s',
            '&:hover': {
              backgroundColor: hexToRgba(blue.primary || 'blue', 0.1)
            }
          }}>
            <div>{columns[n]}</div>
            <div>{item[n]}</div>
          </div>
        )
      })}
    </div>
  );
}
