import styled from '@emotion/styled';

export const Tile = styled.div`
  border-radius: 10px;
  padding: 30px;
  color: #fff;
  cursor: pointer;
  text-align: center;
  font-size: 16px;
  font-weight: 500;
  transition: opacity 0.2s;
  &:hover {
    opacity: .8;
  }
`;
