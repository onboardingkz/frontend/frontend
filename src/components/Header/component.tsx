import React, { FC } from 'react';
import {
  DownloadOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { useHistory, useLocation } from 'react-router-dom';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../store/modules/Auth/atoms';
import { grey } from '@ant-design/colors';
import hexToRgba from 'hex-to-rgba';
import { Container } from '../Container';
import { NavigationItem } from './libs/NavigationItem';
import { Menu } from './libs/Menu';
import { WorkspaceSelect } from './libs/WorkspaceSelect';
import { ProfileMenu } from './libs/ProfileMenu';
import { activeWorkspaceAtom } from '../../store/modules/Organization/atoms';
import logo from '../../assets/images/logo.svg';
import useScroll from 'react-use-scroll';

export const Header: FC = () => {

  const router = useHistory();
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);

  const location = useLocation();
  // @ts-ignore
  const scroll = useScroll();

  return (
    <div
      className={'py-3'}
      css={{
        ...(location.pathname === '/' ? {
          position: 'fixed',
          width: '100%',
          top: 0,
          left: 0,
          backdropFilter: 'blur(5px)',
          zIndex: 999999,
          borderBottom: `1px solid transparent`,
          transition: 'border 0.3s, background 0.3s',
          ...(scroll > 30 ? {
            borderBottom: `1px solid ${grey.primary ? hexToRgba(grey.primary, '.1') : 'grey'}`,
            background: 'rgba(255,255,255,.6)'
          } : {})
        } : {
          borderBottom: `1px solid ${grey.primary ? hexToRgba(grey.primary, '.2') : 'grey'}`,
        })
      }}
    >
      <Container className={'d-flex align-items-center'} size={'100%'}>
        <div>
          <button
            style={{
              background: 'transparent',
              padding: 0,
              border: 'none',
              cursor: 'pointer'
            }}
            className={'ml-2'}
            onClick={() => router.push(token && activeWorkspace ? '/organization' : '/')}
          >
            <img src={logo} style={{ height: 23 }} alt={'Honeycomb logo'} />
          </button>
        </div>

        {token ? (
          <Menu />
        ) : (
          process.env?.REACT_APP_ON_PREMISE === 'true' ? (
            <NavigationItem
              className={'ml-4 h-100'}
              onClick={() => router.push('/download/getting-started')}
              css={{
                border: `1px solid ${hexToRgba(grey.primary || 'grey', 0.2)}`,
              }}
            >
              <DownloadOutlined />
              <span className={'d-inline-block ml-2'}>Download</span>
            </NavigationItem>
          ) : null
        )}

        <div className={'ml-auto'} style={{ display: 'grid', gridTemplateColumns: token ? '1fr 1fr' : '1fr' }}>
          {token ? (
            <>
              <WorkspaceSelect />
              <ProfileMenu />
            </>
          ) : (
            <>
              <NavigationItem
                css={{
                  border: `1px solid ${hexToRgba(grey.primary || 'grey', 0.2)}`
                }}
                onClick={() => router.push('/auth')}
              >
                <UserOutlined />
              </NavigationItem>
            </>
          )}
        </div>
      </Container>
    </div>
  );
}
