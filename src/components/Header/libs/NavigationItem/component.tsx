import styled from "@emotion/styled";
import { blue, red } from '@ant-design/colors';
import hexToRgba from 'hex-to-rgba';
import { Props } from './props';
import { css } from '@emotion/react';

export const NavigationItem = styled.button<Props>`
  border: 0;
  font-size: 13px;
  font-family: inherit;
  cursor: pointer;
  outline: none;
  background-color: transparent;
  border-radius: 10px;
  padding: 10px 15px;
  font-weight: 500;
  transition: background-color 0.3s, color 0.3s;
  ${
    ({ isDanger, active }: Props) => isDanger ? css`
      ${active ? css`
        background-color: ${hexToRgba(red.primary || 'grey', 0.1)};
        color: ${red.primary};
      ` : null}
      &:hover {
        background-color: ${hexToRgba(red.primary || 'grey', 0.1)};
        color: ${red.primary};
      }
    ` : css`
      ${active ? css`
        background-color: ${hexToRgba(blue.primary || 'grey', 0.1)};
        color: ${blue.primary};
      ` : null}
      &:hover {
        background-color: ${hexToRgba(blue.primary || 'grey', 0.1)};
        color: ${blue.primary};
      }
      &:active {
        background-color: ${hexToRgba(blue.primary || 'blue', 0.2)};
      }
    `
  }
`;
