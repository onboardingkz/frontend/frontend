import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLButtonElement> & {
  isDanger?: boolean;
  active?: boolean;
}
