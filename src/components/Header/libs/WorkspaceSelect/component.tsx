import React, { FC, useEffect, useState } from 'react';

import { useHistory } from 'react-router-dom';
import { Card, Divider, Dropdown } from 'antd';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom,
  organizationAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { userOrganizations } from '../../../../store/modules/User/atoms';
import { NavigationItem } from '../NavigationItem';
import { blue, grey } from '@ant-design/colors';
import { DownOutlined } from '@ant-design/icons';
import hexToRgba from 'hex-to-rgba';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';

export const WorkspaceSelect: FC = () => {
  const router = useHistory();
  const [, { set: setActiveWorkspace }] = useAtom(activeWorkspaceAtom);
  const [token] = useAtom(authAtom);
  const [organizations] = useAtom(userOrganizations);
  const [organization, { get: getOrganization }] = useAtom(organizationAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);
  const [title, setTitle] = useState('Choose workspace');

  const handleWorkspaceChange = (id: string) => {
    if (localStorage) {
      localStorage.setItem('workspaceId', `${id};${token}`);
      setActiveWorkspace(id);
      getOrganization(id);
    }
  }

  useEffect(() => {
    setLoading(organization.loading);
    if (!organization.loading) {
      setTitle(organization.data.name);
    }
  }, [organization, setLoading]);

  return (
    <>
      {organizations.data?.length ? (
        <Dropdown trigger={['click']} overlay={(
          <Card
            className={'p-2'}
            css={{
              width: 200,
              borderRadius: 10,
            }}
          >
            <span style={{
              color: grey.primary,
              fontSize: 12,
              padding: '0 15px'
            }}>Change workspace</span>
            <Divider className={'m-0 mt-2 mb-1'} />
            {organizations.data?.map((n: any, i: number) => (
              <NavigationItem
                key={i}
                className={'d-flex align-items-center w-100 text-left'}
                active={organization.data?._id === n._id}
                onClick={organization.data?._id === n._id ? (e) => e.preventDefault() : (() => handleWorkspaceChange(n._id))}
              >
                <span className={'d-inline-flex w-100 align-items-center justify-content-between'}>
                  <span className={'d-block'}>{n.name}</span>
                </span>
              </NavigationItem>
            ))}
            <Divider className={'m-0 mb-1'} />
            <NavigationItem
              className={'d-flex align-items-center w-100 text-left'}
              onClick={() => router.push('/control-panel/organization/create')}
            >
                <span className={'d-inline-flex w-100 align-items-center justify-content-between'}>
                  <span className={'d-block'} style={{ color: blue.primary }}>New organization</span>
                </span>
            </NavigationItem>
          </Card>
        )}>
          <NavigationItem
            className={'ml-3 h-100 d-flex align-items-center justify-content-between'}
            css={{
              border: `1px solid ${hexToRgba(grey.primary || 'grey', 0.2)}`,
            }}
          >
            <span className={'d-inline-block ml-2 mr-2'}>{title}</span>
            <DownOutlined />
          </NavigationItem>
        </Dropdown>
      ) : (
        <NavigationItem
          className={'d-flex align-items-center w-100 text-left'}
          onClick={() => router.push('/control-panel/organization/create')}
        >
            <span className={'d-inline-flex w-100 align-items-center justify-content-between'}>
              <span className={'d-block'} style={{ color: blue.primary }}>New organization</span>
            </span>
        </NavigationItem>
      )}
    </>
  );
}
