import React, { FC } from 'react';
import { Card, Dropdown } from 'antd';
import { NavigationItem } from '../NavigationItem';
import {
  AppleOutlined, FileOutlined, HomeOutlined,
  MenuOutlined,
  OrderedListOutlined,
  ScissorOutlined,
} from '@ant-design/icons';
import hexToRgba from 'hex-to-rgba';
import { grey } from '@ant-design/colors';
import { useHistory, useLocation } from 'react-router-dom';

export const Menu: FC = () => {
  const router = useHistory();
  const location = useLocation();

  const items = [
    {
      path: '/organization',
      title: 'Organization',
      icon: AppleOutlined
    },
    {
      path: '/departments',
      title: 'Departments',
      icon: HomeOutlined
    },
    {
      path: '/instruments',
      title: 'Instruments',
      icon: ScissorOutlined
    },
    {
      path: '/rules',
      title: 'Rules',
      icon: OrderedListOutlined
    },
    {
      path: '/documents',
      title: 'Documents',
      icon: FileOutlined
    },
  ]

  return (
    <>
      <Dropdown trigger={['click']} overlay={(
        <Card
          className={'p-2'}
          css={{
            width: 200,
            borderRadius: 10
          }}
        >
          {items.map((n, i) => (
            <NavigationItem key={i} className={'w-100 text-left'} active={location.pathname === n.path} onClick={() => router.push(n.path)}>
              <n.icon />
              <span className={'d-inline-block ml-3'}>{n.title}</span>
            </NavigationItem>
          ))}
        </Card>
      )}>
        <NavigationItem
          className={'ml-4 h-100'}
          css={{
            border: `1px solid ${hexToRgba(grey.primary || 'grey', 0.2)}`,
          }}
        >
          <MenuOutlined />
        </NavigationItem>
      </Dropdown>
    </>
  );
}
