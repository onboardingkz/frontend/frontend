import React, { FC, useContext, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Avatar, Badge, Card, Divider, Dropdown } from 'antd';
import { NavigationItem } from '../NavigationItem';
import { blue, grey } from '@ant-design/colors';
import { BellOutlined, LogoutOutlined, SettingOutlined } from '@ant-design/icons';
import { getAvatar, getMethod, isOwner } from '../../../../core/utils';
import { SocketConnection } from '../../../SocketConnection';
import { useAtom } from '@reatom/react';
import { authAtom, authUserAtom } from '../../../../store/modules/Auth/atoms';
import { organizationAtom } from '../../../../store/modules/Organization/atoms';
import hexToRgba from 'hex-to-rgba';

export const ProfileMenu: FC = () => {
  const router = useHistory();
  const [user] = useAtom(authUserAtom);
  const [organization] = useAtom(organizationAtom);
  const [token] = useAtom(authAtom);
  const [notifications, setNotifications] = useState<ReadonlyArray<any>>([]);

  const socket = useContext(SocketConnection);

  const location = useLocation();

  const isActive = (path: string, ifMethod = false) => (ifMethod ? getMethod(location.pathname) : location.pathname) === path;

  useEffect(() => {
    socket?.on('NOTIFY', (notification) => {
      setNotifications(prev => [...prev, notification]);
    });

    socket?.on('CLEAR', () => {
      setNotifications([]);
    });
  }, [socket]);

  useEffect(() => {
    const newNotifications = user?.data?.notifications?.filter((n: any) => !n.read);
    if (newNotifications?.length) {
      setNotifications(newNotifications);
    }
  }, [token, user]);

  const isOwn = isOwner(organization, user);

  return (
    <>
      <Dropdown trigger={['click']} overlay={(
        <Card
          className={'p-2'}
          css={{
            width: 300,
            borderRadius: 10,
          }}
        >
          <NavigationItem className={'w-100 text-left d-flex align-items-center'} style={{ gap: 15 }}>
            <div>
              <Avatar size={'large'} src={getAvatar(user.data?.avatar)} style={{ backgroundColor: blue.primary }} />
            </div>
            <div>
              <span className={'d-block'} style={{ fontSize: 18 }}>{user?.data?.name || 'Name'}</span>
              <span className={'d-inline-block'} style={{ color: grey.primary, fontSize: 13 }}>{user?.data?.email || 'Your email'}</span>
            </div>
          </NavigationItem>
          <div style={{ padding: '7px 10px' }}>
            <Divider className={'m-0'} />
          </div>

          {isOwn ? (
            <>
              <NavigationItem className={'w-100 text-left'} active={isActive('control-panel', true)} onClick={() => router.push('/control-panel')}>
                <SettingOutlined />
                <span className={'d-inline-block ml-3'}>Control panel</span>
              </NavigationItem>
            </>
          ) : null}
          <NavigationItem className={'d-flex align-items-center w-100 text-left'} active={isActive('/user/notifications')} onClick={() => router.push('/user/notifications')}>
            <BellOutlined />
            <span className={'d-inline-flex w-100 ml-3 align-items-center justify-content-between'}>
                <span className={'d-block'}>Notifications</span>
                <Badge count={notifications.length} />
            </span>
          </NavigationItem>
          <div style={{ padding: '7px 10px' }}>
            <Divider className={'m-0'} />
          </div>
          <NavigationItem className={'w-100 text-left'} active={isActive('/user/settings')} onClick={() => router.push('/user/settings')}>
            <SettingOutlined />
            <span className={'d-inline-block ml-3'}>Settings</span>
          </NavigationItem>
          <NavigationItem className={'w-100 text-left'} isDanger active={isActive('/auth/logout')} onClick={() => router.push('/auth/logout')}>
            <LogoutOutlined />
            <span className={'d-inline-block ml-3'}>Sign out</span>
          </NavigationItem>
        </Card>
      )}>
        <NavigationItem
          className={'ml-3 h-100 d-flex align-items-center'}
          css={{
            border: `1px solid ${hexToRgba(grey.primary || 'grey', 0.2)}`,
          }}
        >
          <Badge dot={notifications.length > 0}>
            <Avatar style={{ backgroundColor: blue.primary }} size={'small'} src={getAvatar(user?.data?.avatar)} />
          </Badge>
          <span className={'d-inline-block ml-3'}>{user?.data?.name || 'Your name'}</span>
        </NavigationItem>
      </Dropdown>
    </>
  );
}
