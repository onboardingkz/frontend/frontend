import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/index.css';
import "react-multi-carousel/lib/styles.css";
import 'antd/dist/antd.css';
import 'animate.css';

import 'gotham-fonts/css/gotham-rounded.css';
import 'react-pdf/dist/esm/Page/AnnotationLayer.css';

import 'bootstrap-4-grid';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { pdfjs } from 'react-pdf';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
