import React, { FC, useEffect } from 'react';
import { Listing } from '../../pages/Listing';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom, organizationDocumentsAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom, authUserAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { cdn } from '../../../../core/api';
import { getAvatar } from '../../../../core/utils';
import { PDFDocument } from 'pdf-lib';
import { signatureDimensions } from '../../../../core/mappings';
import { Requests as CdnRequests } from '../../../../store/modules/CDN/requests';
import { Requests as SignedDocumentRequests } from '../../../../store/modules/SignedDocument/requests';
import { message } from 'antd';
import { useHistory } from 'react-router-dom';

export const ListingContainer: FC = () => {

  const [documents, { get: getDocuments }] = useAtom(organizationDocumentsAtom);
  const [user] = useAtom(authUserAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getDocuments(activeWorkspace);
    }
  }, [activeWorkspace, getDocuments, token]);

  useEffect(() => {
    setLoading(documents.loading);

    if (!documents.loading && documents.error) {
      throw documents.error;
    }
  }, [documents, setLoading]);

  const loadedSuccessfully = !documents.loading && !documents.error;

  const router = useHistory();

  const handleSign = (item: any) => {
    cdn.get(getAvatar(item.file), {
      responseType: 'arraybuffer',
    })
      .then(res => res.data)
      .then(docPayload => {
        cdn.get(getAvatar(user.data.signature), {
          responseType: 'arraybuffer',
        })
          .then(res => res.data)
          .then(async (signaturePayload) => {
            const pdfDoc = await PDFDocument.load(docPayload);
            const signature = await pdfDoc.embedPng(signaturePayload);

            item.signaturePosition.forEach((n: any) => {
              const page = pdfDoc.getPage(n.page);
              page.drawImage(signature, {
                x: page.getWidth() * (n.x / 100),
                y: page.getHeight() - signatureDimensions.height - (page.getHeight()  * (n.y / 100)),
                width: signatureDimensions.width,
                height: signatureDimensions.height,
              });
            });

            const pdfSignedBytes = await pdfDoc.save();
            const pdfSigned = new File([pdfSignedBytes], `${item.title}_${user.data._id}.pdf`, { type: 'application/pdf' })

            const form = new FormData();
            form.append('file', pdfSigned);
            CdnRequests.upload(form, 'signedDocuments')
              .then(res => {
                const signedDocument = res.data;
                SignedDocumentRequests.create({ file: signedDocument, documentId: item._id })
                  .then(async () => {
                    getDocuments(activeWorkspace);
                    router.push('/signed-documents');
                    await message.success("Successfully signed!");
                  })
                  .catch(async () => {
                    await CdnRequests.delete(signedDocument);
                    message.error("Something happened. Try again later");
                  });
              })
              .catch(async () => await message.error('Error'));
          });
      })
  }

  return loadedSuccessfully ? (
    <Listing documents={documents.data} handleSign={handleSign} />
  ) : null;
}
