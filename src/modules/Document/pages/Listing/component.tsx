import React, { FC, useRef, useState } from 'react';
import { Container } from "../../../../components/Container";
import { Props } from './props';
import { Button, message, Modal, PageHeader, Table, Typography } from 'antd';
import { getAvatar, isOwner } from '../../../../core/utils';
import { useAtom } from '@reatom/react';
import { organizationAtom } from '../../../../store/modules/Organization/atoms';
import { authUserAtom } from '../../../../store/modules/Auth/atoms';
import { Document, Page } from 'react-pdf';
import { green, orange } from '@ant-design/colors';
import hexToRgba from 'hex-to-rgba';
import { useHistory } from 'react-router-dom';
import { signatureDimensions } from '../../../../core/mappings';
import {
  AlibabaOutlined,
  CheckCircleFilled,
} from '@ant-design/icons';

export const Listing: FC<Props> = ({ documents, frame, additionalColumns = [], handleSign, ...rest }: Props) => {

  const [organization] = useAtom(organizationAtom);
  const [user] = useAtom(authUserAtom);

  const [signModal, setSignModal] = useState(false);
  const [document, setDocument] = useState<any>(null)
  const [numPages, setNumPages] = useState(1);

  const router = useHistory();

  const ifOwn = isOwner(organization, user);

  const openModal = (item: any) => {
    setDocument(item);
    setSignModal(true);
  };

  const handleNoSignature = () => {
    message.warn("You have to add signature to sign documents");
    router.push('/user/settings');
  }

  const checkIfSigned = (id: string) => {
    return (user.data?.signedDocuments || []).filter((n: any) => n?.document?._id === id).length > 0;
  }

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    ...(!ifOwn ? [
      {
        title: "Signature",
        key: "sign",
        render: (item: any) => {
          return !checkIfSigned(item._id) ? (
            <Button className={'d-flex align-items-center'} onClick={() => { user.data?.signature ? openModal(item) : handleNoSignature() }}>
              <span>Sign</span>
              <AlibabaOutlined className={'ml-2'} style={{ fontSize: 18 }} />
            </Button>
          ) : (
            <div style={{
              color: green.primary
            }}>
              <CheckCircleFilled className={'mr-2'} />
              <span>Signed</span>
            </div>
          );
        }
      }
    ] : [
      {
        title: "File",
        dataIndex: "file",
        key: "file",
        render: (item: any) => {
          return <a href={getAvatar(item)} target={'_blank'} rel={'noreferrer'}>Open document</a>;
        }
      },
    ]),
    ...additionalColumns
  ];

  return (
    <div className={'animate__animated animate__fadeIn'} {...rest}>
      <Container>
        {!frame ? (
          <PageHeader
            className={'p-0'}
            style={{ marginTop: 100 }}
            ghost={false}
            title={<Typography.Title level={3} className={'m-0'}>Documents</Typography.Title>}
          />
        ) : null}
        <Table
          className={'mt-2'}
          columns={columns}
          dataSource={documents}
          bordered
          pagination={false}
          locale={{ emptyText: 'Organization hasn\'t got any document yet' }}
        />
      </Container>

      {handleSign ? (
        <Modal width={1200} visible={signModal} onCancel={() => setSignModal(false)} title={`Sign: ${document?.title}`} okText={'Sign document'} onOk={() => handleSign(document)}>
          <div className="text-center">
            <div style={{
              display: 'inline-block',
              position: 'relative'
            }}>
              <Document
                file={{ url: getAvatar(document?.file) }}
                onLoadSuccess={({ numPages }) => {
                  setNumPages(numPages);
                }}
              >
                {Array.from(Array(numPages).keys()).map((n, i) => i !== numPages ? (
                  <Page pageNumber={n+1} key={i} />
                ) : null)}
              </Document>
              <div
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: '100%',
                  border: `2px solid ${orange.primary}`,
                  margin: 'auto'
                }}
              >
                {(document?.signaturePosition || []).map((n: any, i: number) => (
                  <div
                    style={{
                      position: 'absolute',
                      border: `2px solid ${green.primary}`,
                      borderRadius: 4,
                      background: hexToRgba(green.primary || 'green', .3),
                      width: signatureDimensions.width,
                      height: signatureDimensions.height,
                      top: `${((100/numPages)*n.page) + n.y}%`,
                      left: `${n.x}%`,
                      color: 'green'
                    }}
                    key={i}
                  >
                    <img src={getAvatar(user.data?.signature)} alt={'user signature'} style={{ height: '100%' }} />
                  </div>
                ))}

              </div>
            </div>
          </div>
        </Modal>
      ) : null}
    </div>
  );
}
