import {
  GetStartedContainer,
} from './containers';

export const Routes = [
  {
    path: '/getting-started',
    component: GetStartedContainer,
  },
];
