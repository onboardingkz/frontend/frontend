import React, { FC } from 'react';
import { GetStarted } from '../../pages/GetStarted';

export const GetStartedContainer: FC = () => {
  return (
    <GetStarted />
  );
}
