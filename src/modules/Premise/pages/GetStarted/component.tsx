import React, { FC } from 'react';
import { Container } from '../../../../components/Container';
import { Button, Divider, Typography } from 'antd';
import { grey } from '@ant-design/colors';
import { DownloadOutlined } from '@ant-design/icons';

export const GetStarted: FC = () => {
  return (
    <div className={'py-5'}>
      <Container size={1000} className={'py-5'}>
        <Typography.Title>Install self-managed OnBoarding</Typography.Title>
        <Typography.Paragraph style={{ fontSize: 16 }}>
          You want to use OnBoarding in your system locally? You can easily deploy our system to your server to manage it yourself.
          You own database, your own server, your own privacy. We will not collect your data.
        </Typography.Paragraph>
        <Divider />
        <Typography.Paragraph
          className={'m-0'}
          style={{
            color: grey.primary,
            fontSize: 11,
            textTransform: 'uppercase'
          }}
        >Recommended installation method</Typography.Paragraph>
        <Typography.Title
          level={2}
          className={'m-0 mb-3'}
        >Manual installation</Typography.Title>
        <Typography.Paragraph style={{ fontSize: 16 }}>
          This is the recommended method for getting started. This method is quite raw. If you are not friendly with Docker or Kubernetes, you can easily deploy our platform manually.
        </Typography.Paragraph>
        <Typography.Title level={3}>Requirements</Typography.Title>
        <ul>
          <li>NodeJS v.14.0 or newer</li>
          <li>MongoDB v.5.0.3 or newer</li>
          <li>Yarn</li>
          <li>Nginx</li>
        </ul>
        <Typography.Title level={4}>1. Download builds</Typography.Title>
        <Button
          type={'primary'}
          href={'https://gitlab.com/onboardingkz/backend/api/-/archive/main/api-main.zip'}
        >
          <DownloadOutlined />
          Download Server
        </Button>
        <Button
          className={'ml-3'}
          type={'primary'}
          href={'https://gitlab.com/onboardingkz/frontend/frontend/-/archive/main/frontend-main.zip'}
        >
          <DownloadOutlined />
          Download Client
        </Button>

        <Typography.Title
          className={'mt-3'}
          level={4}
        >2. Install <a target={'_blank'} href={'https://www.npmjs.com/package/serve'} rel={'noreferrer'}>serve</a> package globally</Typography.Title>
        <Typography.Paragraph>
          <code>$ npm install -g serve</code>
        </Typography.Paragraph>

        <Typography.Title
          className={'mt-3'}
          level={4}
        >3. Launch listeners</Typography.Title>
        <Typography.Paragraph>
          For simple listening service, use <a target={'_blank'} href={'https://www.npmjs.com/package/serve'} rel={'noreferrer'}>serve</a>
        </Typography.Paragraph>
        <Typography.Paragraph>
          <code>$ serve -s build -p YOUR_PORT</code>
        </Typography.Paragraph>

        <Typography.Title level={5}>Daemonizing (Linux only)</Typography.Title>
        <Typography.Paragraph>
          You can use something like <code>nohup</code> for daemonizing process.
        </Typography.Paragraph>
        <Typography.Paragraph>
          <code>$ nohup serve -s build -p YOUR_PORT &</code>
        </Typography.Paragraph>

        <Divider />

        <Typography.Paragraph
          className={'m-0'}
          style={{
            color: grey.primary,
            fontSize: 11,
            textTransform: 'uppercase'
          }}
        >Launch in container</Typography.Paragraph>
        <Typography.Title
          level={2}
          className={'m-0 mb-3'}
        >Docker installation</Typography.Title>
        <Typography.Paragraph style={{ fontSize: 16 }}>
          If you more into Docker, you can easily deploy our project using our configs.
        </Typography.Paragraph>
        <Typography.Title level={3}>Requirements</Typography.Title>
        <ul>
          <li>Docker</li>
          <li>Docker-compose</li>
        </ul>

        <Typography.Title level={4}>1. Download Docker config project</Typography.Title>
        <Button
          type={'primary'}
          href={'https://gitlab.com/onboardingkz/backend/api/-/archive/main/docker-main.zip'}
        >
          <DownloadOutlined />
          Download
        </Button>

        <Typography.Title className={'mt-4'} level={4}>2. Launch</Typography.Title>
        <Typography.Paragraph style={{ fontSize: 16 }}>
          We use docker-compose to deploy
        </Typography.Paragraph>
        <Typography.Paragraph style={{ fontSize: 16 }}>
          <code>
            $ docker-compose up -d
          </code>
        </Typography.Paragraph>
      </Container>
    </div>
  );
}
