import React, { FC, useEffect } from 'react';
import { Listing } from '../../pages/Listing';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom,
  organizationRulesAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';

export const ListingContainer: FC = () => {

  const [rules, { get: getRules }] = useAtom(organizationRulesAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getRules(activeWorkspace);
    }
  }, [activeWorkspace, getRules, token]);

  useEffect(() => {
    setLoading(rules.loading);

    if (!rules.loading && rules.error) {
      throw rules.error;
    }
  }, [rules, setLoading]);

  const loadedSuccessfully = !rules.loading && !rules.error;

  return loadedSuccessfully ? (
    <Listing rules={rules.data} />
  ) : null;
}
