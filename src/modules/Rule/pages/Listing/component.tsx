import React, { FC } from "react";
import { Container } from "../../../../components/Container";
import { Props } from './props';
import { PageHeader, Table, Typography } from 'antd';

export const Listing: FC<Props> = ({ rules, frame, additionalColumns = [], ...rest }: Props) => {

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    ...additionalColumns
  ];

  return (
    <div className={'animate__animated animate__fadeIn'} {...rest}>
      <Container>
        {!frame ? (
          <PageHeader
            className={'p-0'}
            style={{ marginTop: 100 }}
            ghost={false}
            title={<Typography.Title level={3} className={'m-0'}>Rules</Typography.Title>}
          />
        ) : null}
        <Table
          className={'mt-2'}
          columns={columns}
          dataSource={rules}
          bordered
          pagination={false}
          locale={{ emptyText: 'Organization hasn\'t got any instrument yet' }}
        />
      </Container>
    </div>
  );
}
