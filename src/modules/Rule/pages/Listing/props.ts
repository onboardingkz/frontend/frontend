import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  rules: ReadonlyArray<any>;
  frame?: boolean;
  additionalColumns?: ReadonlyArray<Record<any, any>>;
}
