import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  documents: ReadonlyArray<any>;
  frame?: boolean;
  additionalColumns?: ReadonlyArray<Record<any, any>>;
}
