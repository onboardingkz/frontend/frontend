import React, { FC } from 'react';
import { Container } from "../../../../components/Container";
import { Props } from './props';
import { PageHeader, Table, Typography } from 'antd';
import { getAvatar } from '../../../../core/utils';

export const Listing: FC<Props> = ({ documents, frame, additionalColumns = [], ...rest }: Props) => {
  const columns = [
    {
      title: "Title",
      dataIndex: "document",
      key: "title",
      render: (item: any) => item?.title
    },
    {
      title: "File",
      dataIndex: "file",
      key: "file",
      render: (item: any) => {
        return <a href={getAvatar(item)} target={'_blank'} rel={'noreferrer'}>Open document</a>;
      }
    },
    ...additionalColumns
  ];

  return (
    <div className={'animate__animated animate__fadeIn'} {...rest}>
      <Container>
        {!frame ? (
          <PageHeader
            className={'p-0'}
            style={{ marginTop: 100 }}
            ghost={false}
            title={<Typography.Title level={3} className={'m-0'}>Documents</Typography.Title>}
          />
        ) : null}
        <Table
          className={'mt-2'}
          columns={columns}
          dataSource={documents}
          bordered
          pagination={false}
          locale={{ emptyText: 'Organization hasn\'t got any document yet' }}
        />
      </Container>
    </div>
  );
}
