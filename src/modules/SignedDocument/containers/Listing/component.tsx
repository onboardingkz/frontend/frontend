import React, { FC } from 'react';
import { Listing } from '../../pages/Listing';
import { useAtom } from '@reatom/react';
import { authUserAtom } from '../../../../store/modules/Auth/atoms';

export const ListingContainer: FC = () => {

  const [user] = useAtom(authUserAtom);

  return <Listing documents={user.data?.signedDocuments || []} />;
}
