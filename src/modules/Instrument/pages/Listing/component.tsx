import React, { FC } from "react";
import { Container } from "../../../../components/Container";
import { Props } from './props';
import { Button, PageHeader, Table, Typography } from 'antd';
import { isAccessRequestSent, isOwner } from '../../../../core/utils';
import { useAtom } from '@reatom/react';
import { authUserAtom } from '../../../../store/modules/Auth/atoms';
import { CheckOutlined } from '@ant-design/icons';
import { green } from '@ant-design/colors';
import { organizationAtom } from '../../../../store/modules/Organization/atoms';

export const Listing: FC<Props> = ({ instruments, frame, additionalColumns = [], accessRequest, ...rest }: Props) => {
  const [user] = useAtom(authUserAtom);
  const [organization] = useAtom(organizationAtom);
  const ifOwn = isOwner(organization, user);

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description"
    },
    (!frame && accessRequest && !ifOwn ? {
      title: "Access",
      render: (item: any) => isAccessRequestSent(item?.accessRequests, user.data?._id) ? (
        <span>Pending...</span>
      ) : (item.members.includes(user.data?._id) ? (
        <span style={{ color: green.primary }}>
          <CheckOutlined />
        </span>
      ) : (
        <Button onClick={accessRequest(item._id)}>Send access request</Button>
      ))
    } : {}),
    ...additionalColumns
  ];

  return (
    <div className={'animate__animated animate__fadeIn'} {...rest}>
      <Container>
        {!frame ? (
          <PageHeader
            className={'p-0'}
            style={{ marginTop: 100 }}
            ghost={false}
            title={<Typography.Title level={3} className={'m-0'}>Instruments</Typography.Title>}
          />
        ) : null}
        <Table
          className={'mt-2'}
          columns={columns}
          dataSource={instruments}
          bordered
          pagination={false}
          locale={{ emptyText: 'Organization hasn\'t got any instrument yet' }}
        />
      </Container>
    </div>
  );
}
