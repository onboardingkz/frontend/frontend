import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  instruments: ReadonlyArray<any>;
  frame?: boolean;
  additionalColumns?: ReadonlyArray<Record<any, any>>;
  accessRequest?: (id: string) => () => void;
}
