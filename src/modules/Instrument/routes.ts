import { ListingContainer } from './containers/Listing';

export const Routes = [
  {
    path: '/',
    component: ListingContainer,
  },
];
