import React, { FC, useEffect } from 'react';
import { Listing } from '../../pages/Listing';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom,
  organizationInstrumentsAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/Instrument/requests';
import { message } from 'antd';

export const ListingContainer: FC = () => {

  const [instruments, { get: getInstruments }] = useAtom(organizationInstrumentsAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getInstruments(activeWorkspace);
    }
  }, [activeWorkspace, getInstruments, token]);

  useEffect(() => {
    setLoading(instruments.loading);

    if (!instruments.loading && instruments.error) {
      throw instruments.error;
    }
  }, [instruments, setLoading]);

  const accessRequest = (id: string) => () => {
    setLoading(true);
    Requests.sendAccessRequest(id)
      .then(async () => {
        await getInstruments(activeWorkspace);
        await message.success('Access request successfully sent! You will be notified when the status changes');
      })
      .finally(() => setLoading(false));
  }

  const loadedSuccessfully = !instruments.loading && !instruments.error;

  return loadedSuccessfully ? (
    <Listing instruments={instruments.data} accessRequest={accessRequest} />
  ) : null;
}
