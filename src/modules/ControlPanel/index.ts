import { Routes } from './routes';
import { Template } from './template';

export const ControlPanelRoutes = Routes;
export const ControlPanelTemplate = Template;
