import { RulesContainer } from './containers/Rules';
import { IndexContainer } from './containers/Index';
import { InstrumentsContainer } from './containers/Instruments';
import { MembersContainer } from './containers/Members';
import { DepartmentsContainer } from './containers/Departments';
import { OrganizationContainer } from './containers/Organization';
import { DocumentsContainer } from './containers/Documents';
import { SignedDocumentsContainer } from './containers/SignedDocuments';

export const Routes = [
  {
    path: '/',
    component: IndexContainer,
  },
  {
    path: '/members',
    component: MembersContainer,
  },
  {
    path: '/rules',
    component: RulesContainer,
  },
  {
    path: '/documents',
    component: DocumentsContainer,
  },
  {
    path: '/signed-documents',
    component: SignedDocumentsContainer,
  },
  {
    path: '/departments',
    component: DepartmentsContainer,
  },
  {
    path: '/instruments',
    component: InstrumentsContainer,
  },
  {
    path: '/organization',
    component: OrganizationContainer
  },
  {
    path: '/organization/create',
    component: OrganizationContainer,
    template: null
  }
];
