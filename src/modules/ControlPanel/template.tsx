import React, { FC, HTMLAttributes, useEffect } from 'react';
import { List, message, Typography } from 'antd';
import { grey } from '@ant-design/colors';
import { useHistory, useLocation } from 'react-router-dom';
import hexToRgba from 'hex-to-rgba';
import { NavigationItem } from '../../components/Header/libs/NavigationItem';
import { useAtom } from '@reatom/react';
import { organizationAtom } from '../../store/modules/Organization/atoms';
import { authUserAtom } from '../../store/modules/Auth/atoms';
import {
  ApartmentOutlined,
  AppleOutlined, FileOutlined,
  HomeOutlined,
  OrderedListOutlined, PaperClipOutlined, ScissorOutlined,
  UserOutlined,
} from '@ant-design/icons';

type Props = HTMLAttributes<HTMLDivElement>;

export const Template: FC<Props> = ({ children, ...rest }: Props) => {
  const router = useHistory();

  const location = useLocation();

  const [organization] = useAtom(organizationAtom);
  const [user] = useAtom(authUserAtom);

  const navigation = [
    {
      title: 'Home',
      path: '/control-panel',
      icon: HomeOutlined
    },
    {
      title: 'Organization',
      path: '/control-panel/organization',
      icon: AppleOutlined
    },
    {
      title: 'Departments',
      path: '/control-panel/departments',
      icon: ApartmentOutlined
    },
    {
      title: 'Members',
      path: '/control-panel/members',
      icon: UserOutlined
    },
    {
      title: 'Rules',
      path: '/control-panel/rules',
      icon: OrderedListOutlined
    },
    {
      title: 'Documents',
      path: '/control-panel/documents',
      icon: FileOutlined
    },
    {
      title: 'Signed Documents',
      path: '/control-panel/signed-documents',
      icon: PaperClipOutlined
    },
    {
      title: 'Instruments',
      path: '/control-panel/instruments',
      icon: ScissorOutlined
    },
  ];

  useEffect(() => {
    if (organization.data.admin) {
      if (organization.data.admin !== user.data._id) {
        message.error('You have no permission to this section');
        router.push('/organization');
      }
    }
  }, [organization, organization.data.admin, router, user.data._id]);

  return (
    <div className={'h-100'} {...rest}>
      <div
        className={'d-flex h-100'}
      >
        {organization.data && organization.data.admin === user.data._id ? (
          <div
            style={{
              padding: 5,
              paddingTop: 5,
              maxWidth: 210,
              width: '100%',
              borderRight: `1px dashed ${hexToRgba(grey.primary || 'grey', 0.2)}`
            }}
          >
            <List
              itemLayout={'horizontal'}
              dataSource={navigation}
              renderItem={item => (
                <List.Item className={'py-1'}>
                  <NavigationItem onClick={() => router.push(item.path)} active={location.pathname === item.path} className={'w-100 text-left'}>
                    {item.icon ? (<item.icon className={'mr-3'} />) : null}
                    {item.title}
                  </NavigationItem>
                </List.Item>
              )}
            />
          </div>
        ) : null}
        <div className={'w-100'}>
          {children}
        </div>
      </div>
    </div>
  );
}
