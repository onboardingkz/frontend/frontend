export type Props = {
  visible: boolean;
  setVisible: (newVisible: boolean) => void;
  handleCreate: (data: any) => void;
  handleUpdate: (id: string, data: any) => void;
  item: any;
  setItem: (newItem: any) => void;
};
