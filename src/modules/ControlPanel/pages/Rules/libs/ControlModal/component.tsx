import React, { FC, useEffect, useState } from 'react';
import { Props } from './props';
import { Form, Input, Modal } from 'antd';

export const ControlModal: FC<Props> = ({ visible, setVisible, handleCreate, handleUpdate, item, setItem }: Props) => {
  const [title, setTitle] = useState('');

  const data = {
    title
  };

  const handleRuleCreate = () => {
    handleCreate(data);
    setVisible(false);
  };

  const handleRuleUpdate = () => {
    handleUpdate(item._id, data);
    setItem(null);
    setVisible(false);
  }

  useEffect(() => {
    if (item?._id) {
      setTitle(item.title);
    }
  }, [item]);

  return (
    <Modal
      title="Create rule"
      visible={visible}
      onOk={item ? handleRuleUpdate : handleRuleCreate}
      onCancel={() => setVisible(false)}
    >
      <Form.Item>
        <Input size={'middle'} value={title} type={"text"} placeholder={"Type your rule"} onChange={(e) => setTitle(e.currentTarget.value)} />
      </Form.Item>
    </Modal>
  );
}
