import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  rules: ReadonlyArray<any>;
  handleCreate: (data: any) => void;
  handleUpdate: (id: string, data: any) => void;
  handleDelete: (id: string) => () => void;
}
