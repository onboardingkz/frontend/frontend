import React, { FC, useState } from 'react';
import { Props } from './props';
import { Listing } from '../../../Rule/pages/Listing';
import { Button, PageHeader, Popconfirm, Typography } from 'antd';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Container } from '../../../../components/Container';
import { ControlModal } from './libs/ControlModal';

export const Rules: FC<Props> = ({ rules, handleDelete, handleCreate, handleUpdate }: Props) => {
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [editingItem, setEditingItem] = useState(null);

  return (
    <>
      <Container>
        <PageHeader
          className={'p-0 mt-4'}
          ghost={false}
          title={<Typography.Title level={3} className={'mb-3'}>Rules</Typography.Title>}
          extra={(
            <>
              <Button type={'primary'} onClick={() => setIsCreateModalOpen(true)}>
                <PlusOutlined />
              </Button>
            </>
          )}
        />
      </Container>

      <Listing
        rules={rules}
        frame
        additionalColumns={[
          {
            title: 'Actions',
            width: 30,
            render: (item: any) => (
              <div className={'text-center d-flex align-items-center'} style={{ gap: 10 }}>
                <Button
                  size={'small'}
                  onClick={() => {
                    setIsCreateModalOpen(true);
                    setEditingItem(item);
                  }}
                >
                  <EditOutlined />
                </Button>

                <Popconfirm
                  placement="topRight"
                  title={"Are you sure?"}
                  onConfirm={handleDelete(item._id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button
                    size={'small'}
                    danger
                  >
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </div>
            )
          }
        ]}
      />

      <ControlModal
        visible={isCreateModalOpen}
        setVisible={setIsCreateModalOpen}
        handleCreate={handleCreate}
        handleUpdate={handleUpdate}
        item={editingItem}
        setItem={setEditingItem}
      />
    </>
  );
}
