import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  departments: ReadonlyArray<any>;
  handleCreate: (data: any) => void;
  handleUpdate: (id: string, data: any) => void;
  handleDelete: (id: string) => () => void;
}
