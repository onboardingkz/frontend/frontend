import React, { FC, useEffect, useState } from 'react';
import { Props } from './props';
import { Form, Input, Modal, Table, Typography } from 'antd';
import { useAtom } from '@reatom/react';
import { organizationDepartmentsAtom } from '../../../../../../store/modules/Organization/atoms/organizationDepartmentsAtom';

export const ControlModal: FC<Props> = ({ visible, setVisible, handleCreate, handleUpdate, item, setItem }: Props) => {
  const [title, setTitle] = useState('');
  const [departments] = useAtom(organizationDepartmentsAtom);
  const [parent, setParent] = useState<any>(null);

  const data = {
    title,
    ...(parent ? { parentId: parent} : {})
  };

  const handleDepartmentCreate = () => {
    handleCreate(data);
    reset();
  };

  const handleDepartmentUpdate = () => {
    handleUpdate(item._id, data);
    setItem(null);
    reset();
  }

  const reset = () => {
    setParent(null);
    setTitle('');
    setVisible(false);
  }

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
  ];

  const rowSelection = {
    selectedRowKeys: parent ? [parent] : [],
    onChange: (selectedRowKeys: React.Key[]) => {
      if (selectedRowKeys.length) {
        setParent(selectedRowKeys[0]);
      }
    },
  };

  useEffect(() => {
    if (item?._id) {
      setTitle(item.title);
      setParent(item.parent);
    }
  }, [item]);

  return (
    <Modal
      title="Create department"
      visible={visible}
      onOk={item ? handleDepartmentUpdate : handleDepartmentCreate}
      onCancel={reset}
    >
      <Form.Item>
        <Input size={'middle'} value={title} type={"text"} placeholder={"Name of department"} onChange={(e) => setTitle(e.currentTarget.value)} />
      </Form.Item>
      <Form.Item>
        <Typography.Title level={5}>Parent</Typography.Title>
        <Table
          className={'mt-2'}
          columns={columns}
          dataSource={departments.data}
          bordered
          rowSelection={{
            type: 'radio',
            ...rowSelection
          }}
          pagination={false}
          locale={{ emptyText: 'Organization hasn\'t got any departments yet' }}
        />
      </Form.Item>
    </Modal>
  );
}
