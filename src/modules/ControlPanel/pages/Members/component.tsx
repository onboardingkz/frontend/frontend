import React, { FC, useState } from 'react';
import { Props } from './props';
import { Button, PageHeader, Popconfirm, Table, Typography } from 'antd';
import {
  DeleteOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import { Container } from '../../../../components/Container';
import { ControlModal } from './libs/ControlModal';

export const Members: FC<Props> = ({ members, handleDelete, handleAdd, search }: Props) => {

  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email"
    },
    {
      title: "Position",
      dataIndex: "position",
      key: "position"
    },
    {
      title: 'Actions',
      width: 30,
      render: (item: any) => (
        <div className={'text-center d-flex align-items-center'} style={{ gap: 10 }}>
          <Popconfirm
            placement="topRight"
            title={"Are you sure?"}
            onConfirm={handleDelete(item._id)}
            okText="Yes"
            cancelText="No"
          >
            <Button
              size={'small'}
              danger
            >
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </div>
      )
    }
  ];

  return (
    <>
      <Container>
        <PageHeader
          className={'p-0 mt-4'}
          ghost={false}
          title={<Typography.Title level={3} className={'mb-3'}>Members</Typography.Title>}
          extra={(
            <>
              <Button type={'primary'} onClick={() => setIsCreateModalOpen(true)}>
                <PlusOutlined />
              </Button>
            </>
          )}
        />
        <Table
          className={'mt-2'}
          columns={columns}
          dataSource={members}
          bordered
          pagination={false}
          locale={{ emptyText: 'Organization hasn\'t got any members yet' }}
        />

        <ControlModal
          visible={isCreateModalOpen}
          setVisible={setIsCreateModalOpen}
          handleAdd={handleAdd}
          search={search}
        />
      </Container>
    </>
  );
}
