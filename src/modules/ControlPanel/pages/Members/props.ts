import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  members: ReadonlyArray<any>;
  handleAdd: (id: string, data: any) => void;
  handleDelete: (id: string) => () => void;
  search: (term: string) => Promise<any>;
}
