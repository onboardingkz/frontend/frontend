import React, { FC, useEffect, useState } from 'react';
import { Props } from './props';
import { Form, Modal, Select } from 'antd';
import { useAtom } from '@reatom/react';
import { activeWorkspaceAtom } from '../../../../../../store/modules/Organization/atoms';
import { useDebouncedCallback } from 'use-debounce';
import { CloseOutlined, SearchOutlined } from '@ant-design/icons';
import { loadingAtom } from '../../../../../../store/ui/RootTemplate/atoms';
import { grey, red } from '@ant-design/colors';
import { UserCard } from '../../../../../../components/UserCard';
import { userStore } from '../../../../../../store';
import hexToRgba from 'hex-to-rgba';

export const ControlModal: FC<Props> = ({ visible, setVisible, handleAdd, search }: Props) => {
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [searchResult, setSearchResult] = useState([]);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  const [userId, setUserId] = useState<string | null>(null);
  const [user, setUser] = useState<any>(null);

  const data = {
    userId
  };

  const handleAddMember = () => {
    handleAdd(activeWorkspace, data);
    setVisible(false);
  };

  const handleSearch = useDebouncedCallback((term: string) => {
    setLoading(true);
    setUserId(null);
    search(term)
      .then((res) => {
        setSearchResult(res.data || []);
      })
      .finally(() => setLoading(false));
  }, 300);

  const handleUserSelect = (value: string) => {
    setUserId(value || null);
    setLoading(true);
    userStore.requests.Requests.get(value)
      .then(res => {
        setUser(res.data);
      })
      .finally(() => setLoading(false));
  };

  const reset = () => {
    setUser(null);
    setUserId(null);
    setSearchResult([]);
  }

  useEffect(() => {
    setSearchResult([]);
    setUserId(null);
  }, []);

  return (
    <Modal
      title="Add member"
      visible={visible}
      onOk={handleAddMember}
      onCancel={() => setVisible(false)}
    >
        {user ? (
          <div className={'position-relative py-2 px-3'} style={{ border: `1px solid ${hexToRgba(grey.primary || 'grey', 0.1)}`, borderRadius: 10 }}>
            <button
              css={{
                position: 'absolute',
                border: 0,
                top: -12,
                right: -12,
                backgroundColor: red.primary,
                color: '#fff',
                width: 24,
                height: 24,
                textAlign: 'center',
                borderRadius: '100%',
                cursor: 'pointer',
                fontSize: 10,
                outline: 'none',
                transition: 'transform 0.2s',
                '&:hover': {
                  transform: 'scale(1.1)'
                }
              }}
              onClick={reset}
            >
              <CloseOutlined />
            </button>
            <UserCard
              item={user}
            />
          </div>

        ) : (
          <Form.Item>
            <Select
              showSearch
              defaultActiveFirstOption={false}
              showArrow={false}
              onChange={handleUserSelect}
              filterOption={false}
              suffixIcon={<SearchOutlined />}
              allowClear
              placeholder={'Search user by email, phone or name...'}
              onSearch={handleSearch}
            >
              {searchResult.map((n: any) => (
                <Select.Option value={n._id}>
                  <UserCard item={n} />
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        )}
    </Modal>
  );
}
