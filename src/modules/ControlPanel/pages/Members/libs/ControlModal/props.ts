export type Props = {
  visible: boolean;
  setVisible: (newVisible: boolean) => void;
  handleAdd: (id: string, data: any) => void;
  search: (term: string) => Promise<any>;
};
