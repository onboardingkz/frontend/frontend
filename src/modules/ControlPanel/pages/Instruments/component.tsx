import React, { FC, useState } from 'react';
import { Props } from './props';
import { Listing } from '../../../Instrument/pages/Listing';
import { Badge, Button, PageHeader, Popconfirm, Tooltip, Typography } from 'antd';
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  PullRequestOutlined,
} from '@ant-design/icons';
import { Container } from '../../../../components/Container';
import { ControlModal } from './libs/ControlModal';
import { AccessRequestsModal } from './libs/AccessRequestsModal';

export const Instruments: FC<Props> = ({ instruments, handleDelete, handleCreate, handleUpdate, handleAccept, handleDecline }: Props) => {
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [isAccessRequestModalOpen, setIsAccessRequestModalOpen] = useState(false);
  const [editingItem, setEditingItem] = useState(null);

  return (
    <>
      <Container>
        <PageHeader
          className={'p-0 mt-4'}
          ghost={false}
          title={<Typography.Title level={3} className={'mb-3'}>Instruments</Typography.Title>}
          extra={(
            <>
              <Button type={'primary'} onClick={() => setIsCreateModalOpen(true)}>
                <PlusOutlined />
              </Button>
            </>
          )}
        />
      </Container>

      <Listing
        instruments={instruments}
        frame
        additionalColumns={[
          {
            title: 'Actions',
            width: 30,
            render: (item: any) => (
              <div className={'text-center d-flex align-items-center'} style={{ gap: 10 }}>
                <Tooltip title={"Access requests"}>
                  <Badge count={item.accessRequests?.length}>
                    <Button
                      size={'small'}
                      onClick={() => {
                        setEditingItem(item);
                        setIsAccessRequestModalOpen(true);
                      }}
                    >
                      <PullRequestOutlined />
                    </Button>
                  </Badge>
                </Tooltip>

                <Button
                  size={'small'}
                  onClick={() => {
                    setIsCreateModalOpen(true);
                    setEditingItem(item);
                  }}
                >
                  <EditOutlined />
                </Button>

                <Popconfirm
                  placement="topRight"
                  title={"Are you sure?"}
                  onConfirm={handleDelete(item._id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button
                    size={'small'}
                    danger
                  >
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </div>
            )
          }
        ]}
      />

      <ControlModal
        visible={isCreateModalOpen}
        setVisible={setIsCreateModalOpen}
        handleCreate={handleCreate}
        handleUpdate={handleUpdate}
        item={editingItem}
        setItem={setEditingItem}
      />

      <AccessRequestsModal
        visible={isAccessRequestModalOpen}
        setVisible={setIsAccessRequestModalOpen}
        handleAccept={handleAccept}
        handleDecline={handleDecline}
        item={editingItem}
        setItem={setEditingItem}
      />
    </>
  );
}
