export type Props = {
  visible: boolean;
  setVisible: (newVisible: boolean) => void;
  handleAccept: (id: string) => void;
  handleDecline: (id: string) => void;
  item: any;
  setItem: (newItem: any) => void;
};
