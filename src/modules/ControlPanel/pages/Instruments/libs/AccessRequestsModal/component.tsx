import React, { FC } from 'react';
import { Props } from './props';
import { Button, Modal, Popconfirm, Table } from 'antd';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { UserCard } from '../../../../../../components/UserCard';

export const AccessRequestsModal: FC<Props> = ({ visible, setVisible, handleAccept, handleDecline, item, setItem }: Props) => {
  const handleAccessRequestAccept = (id: string) => () => {
    handleAccept(id);
    setVisible(false);
  };

  const handleAccessRequestDecline = (id: string) => () => {
    handleDecline(id);
    setItem(null);
    setVisible(false);
  }

  const columns = [
    {
      title: "From",
      render: (accessRequest: any) => (
        <UserCard item={accessRequest.from} />
      )
    },
    {
      title: 'Actions',
      width: 30,
      render: (accessRequest: any) => (
        <div className={'text-center d-flex align-items-center'} style={{ gap: 10 }}>
          <Popconfirm
            placement="topRight"
            title={"Are you sure?"}
            onConfirm={handleAccessRequestAccept(accessRequest._id)}
            okText="Yes"
            cancelText="No"
          >
            <Button
              size={'small'}
              type={'primary'}
            >
              <CheckOutlined />
            </Button>
          </Popconfirm>
          <Popconfirm
            placement="topRight"
            title={"Are you sure?"}
            onConfirm={handleAccessRequestDecline(accessRequest._id)}
            okText="Yes"
            cancelText="No"
          >
            <Button
              size={'small'}
              danger
            >
              <CloseOutlined />
            </Button>
          </Popconfirm>
        </div>
      )
    }
  ];

  return (
    <Modal
      title={`Access Requests to ${item?.title}`}
      visible={visible}
      onOk={() => setVisible(false)}
      onCancel={() => setVisible(false)}
    >
      <Table
        className={'mt-2'}
        columns={columns}
        dataSource={item?.accessRequests}
        bordered
        pagination={false}
        locale={{ emptyText: 'There is no access requests yet' }}
      />
    </Modal>
  );
}
