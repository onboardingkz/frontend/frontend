import React, { FC, useEffect, useRef, useState } from 'react';
import { Props } from './props';
import { Container } from '../../../../components/Container';
import {
  Avatar,
  Button,
  Form, Input,
  Popconfirm,
  Spin,
  Typography,
} from 'antd';
import { getAvatar } from '../../../../core/utils';
import { blue } from '@ant-design/colors';
import hexToRgba from 'hex-to-rgba';
import { CameraOutlined } from '@ant-design/icons';

export const Organization: FC<Props> = ({ handleCreate, handleUpdate, handleUpload, item, loading, handleDelete, ...rest }: Props) => {
  const [name, setName] = useState('');
  const [bin, setBin] = useState('');
  const [address, setAddress] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');


  const [avatar, setAvatar] = useState<any>(null);

  const avatarInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;


  const data = {
    ...(item?.name !== name ? { name } : {}),
    ...(item?.bin !== bin ? { bin } : {}),
    ...(item?.address !== address ? { address } : {}),
    ...(item?.phone !== phone ? { phone } : {}),
    ...(item?.email !== email ? { email } : {}),
  };

  useEffect(() => {
    if (item) {
      setName(item.name);
      setBin(item.bin);
      setAddress(item.address);
      setPhone(item.phone);
      setEmail(item.email);
    }
  }, [item]);


  const disabled = () => {
    const validation = name?.length && bin?.length > 5 && address?.length && phone?.length > 9;

    if (item) {
      return Object.keys(data).length === 0 && !avatar;
    }
    return !validation;
  }

  const onClick = () => {
    const form = new FormData();
    if (avatarInputRef.current?.files?.length) {
      const avatarFile = avatarInputRef.current.files[0];
      form.append('file', avatarFile);
      avatarInputRef.current.value = '';
      handleUpload(form, data);
    } else {
      item ? handleUpdate(data) : handleCreate(data)
    }
  }

  return (
    <div className={'animate__animated animate__fadeIn pt-4 h-100'} {...rest}>
      <Container size={500} style={{ marginTop: 80 }}>
        <Typography.Title className={'text-center mb-5'} level={2}>
          {loading ? (<Spin className={'mr-4'} />) : null}
          {item?.name || 'New organization'}
        </Typography.Title>
        <Form layout="vertical">
          {item ? (
            <Form.Item className={'text-center mt-5'}>
              <label style={{
                position: 'relative',
                display: 'inline-block',
                borderRadius: '100%',
                overflow: 'hidden',
              }}>
                <Avatar src={avatarInputRef.current?.files?.[0] ? URL.createObjectURL(avatarInputRef.current?.files?.[0]) : getAvatar(item?.avatar)} size={120} style={{ backgroundColor: blue.primary }} />
                <div css={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: '100%',
                  backgroundColor: hexToRgba('#000', 0.4),
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  color: '#fff',
                  fontSize: 25,
                  cursor: 'pointer',
                  opacity: 0,
                  transition: 'opacity 0.3s',
                  "&:hover": {
                    opacity: 1
                  }
                }}>
                  <CameraOutlined />
                  <input type={'file'} accept="image/*" hidden ref={avatarInputRef} onChange={(e) => setAvatar(e.currentTarget?.files?.[0])} />
                </div>
              </label>
            </Form.Item>
          ) : null}

          <Form.Item label={"Name of the organization"} required>
            <Input disabled={loading} value={name} size={'middle'} placeholder={'Google LLC.'} type={"text"} onChange={(e) => setName(e.currentTarget.value)} />
          </Form.Item>
          <Form.Item label={"BIN"} required>
            <Input disabled={loading} value={bin} size={'middle'} placeholder={'01234567'} type={"text"} onChange={(e) => setBin(e.currentTarget.value)} />
          </Form.Item>
          <Form.Item label={"Address of the organization"} required>
            <Input disabled={loading} value={address} size={'middle'} placeholder={'Makerere Hill Road, Block 9 - Plot 283'} type={"text"} onChange={(e) => setAddress(e.currentTarget.value)} />
          </Form.Item>
          <Form.Item label={"Phone number"} required>
            <Input disabled={loading} value={phone} size={'middle'} placeholder={'+7(777)12312312'} type={"text"} onChange={(e) => setPhone(e.currentTarget.value)} />
          </Form.Item>
          <Form.Item label={"Email address"} required>
            <Input disabled={loading} value={email} size={'middle'} placeholder={'example@example.com'} type={"text"} onChange={(e) => setEmail(e.currentTarget.value)} />
          </Form.Item>
          <Button
            className={'animate__animated animate__lightSpeedInRight mt-3'}
            size={"large"}
            type={"primary"}
            block
            shape={'round'}
            onClick={onClick}
            disabled={disabled()}
          >
            {item ? 'Save' : 'Create'}
          </Button>
          {item ? (
            <Popconfirm
              title={'Are you sure?'}
              onConfirm={() => handleDelete()}
            >
              <Button
                className={'animate__animated animate__lightSpeedInRight mt-2'}
                size={"large"}
                block
                danger
                shape={'round'}
                type={'primary'}
              >Delete</Button>
            </Popconfirm>
          ) : null}
        </Form>
      </Container>
    </div>
  );
};
