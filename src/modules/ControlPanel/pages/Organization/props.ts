import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly handleCreate: (data: any) => void;
  readonly handleUpdate: (data: any) => void;
  readonly handleUpload: (data: any, dataFields: any) => void;
  readonly handleDelete: () => void;
  readonly item?: any;
  readonly loading?: boolean;
};
