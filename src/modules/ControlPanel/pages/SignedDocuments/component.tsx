import React, { FC } from 'react';
import { Props } from './props';
import { Button, Card, Divider, PageHeader, Popconfirm, Typography } from 'antd';
import { DeleteOutlined, PaperClipOutlined } from '@ant-design/icons';
import { Container } from '../../../../components/Container';
import { UserCard } from '../../../../components/UserCard';
import { getAvatar } from '../../../../core/utils';

export const SignedDocuments: FC<Props> = ({ documents, handleDelete }: Props) => {
  return (
    <>
      <Container>
        <PageHeader
          className={'p-0 mt-4'}
          ghost={false}
          title={<Typography.Title level={3} className={'mb-3'}>Signed Documents</Typography.Title>}
        />

        {documents.map((n, i) => (
          <Card className={'mb-2'} key={i}>
            <UserCard item={n.user} />
            <Divider />
            <div className="d-flex" style={{ gap: 20 }}>

              <div>
                <a href={getAvatar(n.file)} target={'_blank'} rel={'noreferrer'}>
                  <PaperClipOutlined /> Signed Document
                </a>
              </div>
              <div className={'mr-auto'}>
                <span>Document: </span>
                <a href={getAvatar(n.document?.file)} target={'_blank'} rel={'noreferrer'}>{n.document?.title}</a>
              </div>
              <div>
                <Popconfirm
                  placement="topRight"
                  title={"Are you sure?"}
                  onConfirm={handleDelete(n._id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button
                    size={'small'}
                    danger
                  >
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </div>
            </div>

          </Card>
        ))}
        {!documents.length ? (
          <span>No signed documents yet</span>
        ) : null}
      </Container>
    </>
  );
}
