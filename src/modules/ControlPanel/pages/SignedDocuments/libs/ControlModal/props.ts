export type Props = {
  visible: boolean;
  setVisible: (newVisible: boolean) => void;
  handleCreate: (data: any) => void;
  setItem: (newItem: any) => void;
};
