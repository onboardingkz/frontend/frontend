import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  documents: ReadonlyArray<any>;
  handleDelete: (id: string) => () => void;
}
