import React, { FC, MouseEvent, useEffect, useRef, useState } from 'react';
import { Props } from './props';
import { Button, Form, Input, message, Modal } from 'antd';
import { Requests as CdnRequests } from '../../../../../../store/modules/CDN/requests';
import { getAvatar } from '../../../../../../core/utils';
import { UploadOutlined } from '@ant-design/icons';
import { Document, Page } from 'react-pdf';
import { blue, green, orange, red } from '@ant-design/colors';
import _ from 'lodash';
import { signatureDimensions } from '../../../../../../core/mappings';
import hexToRgba from 'hex-to-rgba';


export const ControlModal: FC<Props> = ({ visible, setVisible, handleCreate }: Props) => {
  const [title, setTitle] = useState('');
  const [file, setFile] = useState<any>(null);
  const fileInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const [document, setDocument] = useState<string | null>(null);
  const [signature, setSignature] = useState<ReadonlyArray<any>>([]);
  const [numPages, setNumPages] = useState(1);
  const [signaturePosition, setSignaturePosition] = useState<ReadonlyArray<any>>([]);

  const signatureCanvasRef = useRef() as React.MutableRefObject<any>;

  const data = {
    title,
    file: document,
    signaturePosition
  };

  const handleDocumentCreate = () => {
      handleCreate(data);
      setVisible(false);
  };

  const handleDocumentUpload = () => {
    const form = new FormData();
    if (fileInputRef.current.files?.length) {
      form.append('file', fileInputRef.current.files[0]);
      fileInputRef.current.value = '';
      CdnRequests.upload(form, 'documents')
        .then((res) => {
          setDocument(res.data);
        })
    } else {
      message.error('Fill all the fields');
    }
  }

  const handleDocumentDelete = () => {
    if (document) {
      CdnRequests.delete(document)
        .then(() => {
          if (fileInputRef.current) {
            fileInputRef.current.value = '';
            setFile(null);
          }
          setDocument(null);
          setSignature([]);
        });
    }
  }

  useEffect(() => {
    if (fileInputRef.current) {
      fileInputRef.current.value = '';
      setFile(null);
    }
    setTitle('');
    setDocument(null);
  }, []);

  const validation = title?.length && document && signature;

  const cancel = () => {
    handleDocumentDelete();
    setVisible(false);
  }

  const markSignature = (e: MouseEvent<HTMLDivElement>) => {
    let bounds = signatureCanvasRef.current.getBoundingClientRect();
    const pageHeight = bounds.height / numPages;
    let x = e.clientX - bounds.left;
    let y = e.clientY - bounds.top;
    const coords = _.cloneDeep({ x, y })
    setSignature(prev => [...prev, coords]);
    setSignaturePosition(prev => [...prev, {
      x: (coords.x / signatureCanvasRef.current?.clientWidth) * 100,
      y: ((coords.y % pageHeight) / signatureCanvasRef.current?.clientHeight) * 100,
      page: Math.ceil(coords.y / pageHeight) - 1
    }]);
  }

  const deleteSignature = (index: number) => {
    setSignature(prev => prev.filter((n, i) => index !== i));
    setSignaturePosition(prev => prev.filter((n, i) => index !== i));
  }

  return (
    <Modal
      title="Add document"
      visible={visible}
      onOk={handleDocumentCreate}
      onCancel={cancel}
      okButtonProps={{ disabled: !validation }}
      width={1200}
    >
      <Form.Item>
        {!document ? (
          <>
            <input type={'file'} accept=".pdf" ref={fileInputRef} onChange={(e) => setFile(e.currentTarget?.files?.[0])} />
            <Button block className={'mt-3'} disabled={!file} onClick={handleDocumentUpload}>
              <UploadOutlined />
            </Button>
          </>
        ) : (
          <>
            <Input className={'mb-3'} size={'middle'} value={title} type={"text"} placeholder={"Document name"} onChange={(e) => setTitle(e.currentTarget.value)} />
            <div className="text-center">
              <div
                style={{
                  border: `2px solid ${blue.primary}`,
                  display: 'inline-block',
                  position: 'relative'
                }}
              >
                <Document
                  file={{ url: getAvatar(document)}}
                  onLoadSuccess={({ numPages }) => {
                    setNumPages(numPages);
                  }}
                  options={{
                    withCredentials: false
                  }}
                >
                  {Array.from(Array(numPages).keys()).map((n, i) => i !== numPages ? (
                    <Page pageNumber={n+1} key={i} />
                  ) : null)}
                </Document>
                <div
                  ref={signatureCanvasRef}
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    border: `2px solid ${orange.primary}`,
                    margin: 'auto'
                  }}
                >
                  <div onClick={markSignature} style={{ height: '100%', width: '100%' }} />
                  {signature.length ? signature.map((n, i) => (
                    <div
                      css={{
                        position: 'absolute',
                        border: `1px solid ${green.primary}`,
                        width: signatureDimensions.width,
                        height: signatureDimensions.height,
                        top: n.y,
                        left: n.x,
                        color: 'green',
                        background: hexToRgba(green.primary || 'green', .2),
                        fontSize: 12,
                        borderRadius: 4,
                        display: 'flex',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        cursor: 'pointer',
                        transition: 'transform 0.2s, color 0.2s, background 0.2s',
                        '&:hover': {
                          transform: 'scale(1.1)',
                          color: '#fff',
                          background: red.primary,
                          border: 'none',
                          '&::before': {
                            content: "'Delete'"
                          }
                        },
                        '&::before': {
                          content: "'Signature mark'"
                        }
                      }}
                      key={i}
                      onClick={() => deleteSignature(i)}
                    />
                  )) : null}
                </div>
              </div>
            </div>
          </>
        )}
      </Form.Item>
    </Modal>
  );
}

