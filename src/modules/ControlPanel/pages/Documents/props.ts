import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  documents: ReadonlyArray<any>;
  handleCreate: (data: any) => void;
  handleDelete: (id: string) => () => void;
}
