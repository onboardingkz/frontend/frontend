import React, { FC, useState } from 'react';
import { Props } from './props';
import { Listing } from '../../../Document/pages/Listing';
import { Button, PageHeader, Popconfirm, Typography } from 'antd';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Container } from '../../../../components/Container';
import { ControlModal } from './libs/ControlModal';

export const Documents: FC<Props> = ({ documents, handleDelete, handleCreate }: Props) => {
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [editingItem, setEditingItem] = useState(null);

  return (
    <>
      <Container>
        <PageHeader
          className={'p-0 mt-4'}
          ghost={false}
          title={<Typography.Title level={3} className={'mb-3'}>Documents</Typography.Title>}
          extra={(
            <>
              <Button type={'primary'} onClick={() => setIsCreateModalOpen(true)}>
                <PlusOutlined />
              </Button>
            </>
          )}
        />
      </Container>

      <Listing
        documents={documents}
        frame
        additionalColumns={[
          {
            title: 'Actions',
            width: 30,
            render: (item: any) => (
              <div className={'text-center d-flex align-items-center'} style={{ gap: 10 }}>
                <Popconfirm
                  placement="topRight"
                  title={"Are you sure?"}
                  onConfirm={handleDelete(item._id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button
                    size={'small'}
                    danger
                  >
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </div>
            )
          }
        ]}
      />

      <ControlModal
        visible={isCreateModalOpen}
        setVisible={setIsCreateModalOpen}
        handleCreate={handleCreate}
        setItem={setEditingItem}
      />
    </>
  );
}
