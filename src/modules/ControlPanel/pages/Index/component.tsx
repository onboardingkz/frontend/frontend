import React, { FC } from 'react';
import { Props } from './props';
import { Container } from '../../../../components/Container';
import { PageHeader, Typography } from 'antd';

export const Index: FC<Props> = () => {

  return (
    <div className={'animate__animated animate__fadeIn'}>
      <Container>
        <PageHeader
          className={'p-0 mt-4'}
          ghost={false}
          title={<Typography.Title level={3} className={'mb-3'}>Control panel</Typography.Title>}
        />

      </Container>
    </div>
  );
}
