import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom, organizationInstrumentsAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/Instrument/requests';
import { Instruments } from '../../pages/Instruments';

export const InstrumentsContainer: FC = () => {
  const [instruments, { get: getInstruments }] = useAtom(organizationInstrumentsAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getInstruments(activeWorkspace);
    }
  }, [activeWorkspace, getInstruments, token]);

  useEffect(() => {
    setLoading(instruments.loading);

    if (!instruments.loading && instruments.error) {
      throw instruments.error;
    }
  }, [instruments, setLoading]);

  const handleCreate = (data: any) => {
    setLoading(true);
    Requests.create(data)
      .then(() => {
        getInstruments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  };

  const handleUpdate = (id: string, data: any) => {
    setLoading(true);
    Requests.update(id, data)
      .then(() => {
        getInstruments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  };

  const handleDelete = (id: string) => () => {
    setLoading(true);
    Requests.delete(id)
      .then(() => {
        getInstruments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  }

  const handleAccept = (id: string) => {
    setLoading(true);
    Requests.acceptAccessRequest(id)
      .then(() => {
        getInstruments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  }

  const handleDecline = (id: string) => {
    setLoading(true);
    Requests.declineAccessRequest(id)
      .then(() => {
        getInstruments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  }

  const loadedSuccessfully = !instruments.loading && !instruments.error;

  return loadedSuccessfully ? (
    <Instruments
      instruments={instruments.data}
      handleDelete={handleDelete}
      handleCreate={handleCreate}
      handleUpdate={handleUpdate}
      handleAccept={handleAccept}
      handleDecline={handleDecline}
    />
  ) : null;
}
