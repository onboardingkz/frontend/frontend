import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/Organization/requests';
import { organizationMembersAtom } from '../../../../store/modules/Organization/atoms/organizationMembersAtom';
import { Members } from '../../pages/Members';
import { userStore } from '../../../../store';

export const MembersContainer: FC = () => {
  const [members, { get: getMembers }] = useAtom(organizationMembersAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getMembers(activeWorkspace);
    }
  }, [activeWorkspace, getMembers, token]);

  useEffect(() => {
    setLoading(members.loading);

    if (!members.loading && members.error) {
      throw members.error;
    }
  }, [members, setLoading]);

  const handleAdd = (id: string, data: any) => {
    setLoading(true);
    Requests.addMember(id, data)
      .then(() => {
        getMembers(activeWorkspace);
      })
      .finally(() => setLoading(false));
  };

  const handleDelete = (id: string) => () => {
    setLoading(true);
    Requests.deleteMember(activeWorkspace, { userId: id })
      .then(() => {
        getMembers(activeWorkspace);
      })
      .finally(() => setLoading(false));
  }

  const handleSearch = (term: string) => {
    return userStore.requests.Requests.search({term});
  };

  const loadedSuccessfully = !members.loading && !members.error;

  return loadedSuccessfully ? (
    <Members
      members={members.data}
      handleDelete={handleDelete}
      handleAdd={handleAdd}
      search={handleSearch}
    />
  ) : null;
}
