import React, { FC, useEffect } from 'react';
import { Rules } from '../../pages/Rules';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom, organizationDocumentsAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/Document/requests';
import { Documents } from '../../pages/Documents';
import { message } from 'antd';

export const DocumentsContainer: FC = () => {
  const [documents, { get: getDocuments }] = useAtom(organizationDocumentsAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getDocuments(activeWorkspace);
    }
  }, [activeWorkspace, getDocuments, token]);

  useEffect(() => {
    setLoading(documents.loading);

    if (!documents.loading && documents.error) {
      throw documents.error;
    }
  }, [documents, setLoading]);

  const handleCreate = (data: any) => {
    Requests.create({ ...data })
      .then(async () => {
        getDocuments(activeWorkspace);
        await message.success('Successfully added');
      })
      .catch(async () => {
        await message.error('Something happened');
      });
  };

  const handleDelete = (id: string) => () => {
    Requests.delete(id)
      .then(async () => {
        getDocuments(activeWorkspace);
        await message.success('Successfully deleted');
      })
      .catch(async () => {
        await message.error('Something happened');
      });
  }


  const loadedSuccessfully = !documents.loading && !documents.error;

  return loadedSuccessfully ? (
    <Documents
      documents={documents.data}
      handleDelete={handleDelete}
      handleCreate={handleCreate}
    />
  ) : null;
}
