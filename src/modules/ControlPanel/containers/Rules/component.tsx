import React, { FC, useEffect } from 'react';
import { Rules } from '../../pages/Rules';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom,
  organizationRulesAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/Rule/requests';

export const RulesContainer: FC = () => {
  const [rules, { get: getRules }] = useAtom(organizationRulesAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getRules(activeWorkspace);
    }
  }, [activeWorkspace, getRules, token]);

  useEffect(() => {
    setLoading(rules.loading);

    if (!rules.loading && rules.error) {
      throw rules.error;
    }
  }, [rules, setLoading]);

  const handleCreate = (data: any) => {
    setLoading(true);
    Requests.create(data)
      .then(() => {
        getRules(activeWorkspace);
      })
      .finally(() => setLoading(false));
  };

  const handleUpdate = (id: string, data: any) => {
    setLoading(true);
    Requests.update(id, data)
      .then(() => {
        getRules(activeWorkspace);
      })
      .finally(() => setLoading(false));
  };

  const handleDelete = (id: string) => () => {
    setLoading(true);
    Requests.delete(id)
      .then(() => {
        getRules(activeWorkspace);
      })
      .finally(() => setLoading(false));
  }


  const loadedSuccessfully = !rules.loading && !rules.error;

  return loadedSuccessfully ? (
    <Rules
      rules={rules.data}
      handleDelete={handleDelete}
      handleCreate={handleCreate}
      handleUpdate={handleUpdate}
    />
  ) : null;
}
