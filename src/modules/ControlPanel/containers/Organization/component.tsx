import React, { FC, useEffect } from 'react';
import { authStore, organizationStore } from '../../../../store';
import { message } from 'antd';
import { useAtom } from '@reatom/react';
import { useHistory, useLocation } from 'react-router-dom';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import {
  activeWorkspaceAtom,
  organizationAtom,
} from '../../../../store/modules/Organization/atoms';
import { getAction } from '../../../../core/utils';
import { Organization } from '../../pages/Organization';
import { userOrganizations } from '../../../../store/modules/User/atoms';
import { Requests as CdnRequests } from '../../../../store/modules/CDN/requests';

export const OrganizationContainer: FC = () => {
  const router = useHistory();
  const [token] = useAtom(authStore.atoms.authAtom);
  const [loading, { set: setLoading }] = useAtom(loadingAtom);
  const [organization, { get: getOrganization }] = useAtom(organizationAtom);
  const [, { get: getOrganizations }] = useAtom(userOrganizations);
  const [activeWorkspace, { set: setActiveWorkspace }] = useAtom(activeWorkspaceAtom);

  const locationProp = useLocation();
  const action = getAction(locationProp.pathname);

  const id = activeWorkspace;

  useEffect(() => {
    if (id && token) {
      getOrganization(id);
    }
  }, [activeWorkspace, getOrganization, id, token]);

  useEffect(() => {
    setLoading(organization.loading);

    if (!organization.loading && organization.error) {
      throw organization.error;
    }
  }, [organization, setLoading]);

  const handleWorkspaceChange = (id: string | null = null) => {
    if (localStorage) {
      if (id) {
        localStorage.setItem('workspaceId', `${id};${token}`);
        setActiveWorkspace(id);
        getOrganizations();
        getOrganization(id);
      }
      else {
        localStorage.removeItem('workspaceId');
        setActiveWorkspace('');
        // eslint-disable-next-line no-restricted-globals
        location.replace('/');
      }
    }
  }

  const handleDelete = () => {
    setLoading(true);
    organizationStore.requests.Requests.delete(activeWorkspace)
      .then(async () => {
        handleWorkspaceChange();
        await message.success('Successfully deleted!');
      })
      .catch(async () => {
        await message.error('Something wrong!');
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleCreate = (data: any) => {
    setLoading(true);
    organizationStore.requests.Requests.create(data)
      .then((res) => {
        handleWorkspaceChange(res.data._id);
        router.push('/organization');
      })
      .catch(async (err) => {
        if (err?.response) {
          await message.error(err.response?.data?.message);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleUpdate = (data: any) => {
    setLoading(true);
    organizationStore.requests.Requests.update(id, { ...(
      Object.entries(data).reduce((acc: any, [key, value]) => (organization.data?.[key] !== value ? {
        ...acc,
        [key]: value
      } : acc), {})
    ) })
      .then(async () => {
        getOrganization(id);
        await message.success('Saved');
      })
      .catch(err => {
        throw err;
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleUpload = (data: any, dataFields: any) => {
    setLoading(true);
    CdnRequests.upload(data, 'organizations')
      .then((res) => {
        handleUpdate({ ...dataFields, avatar: res.data });
      })
      .finally(() => setLoading(false));
  }

  return !loading ? (
    <Organization
      handleCreate={handleCreate}
      handleUpdate={handleUpdate}
      handleDelete={handleDelete}
      handleUpload={handleUpload}
      item={action !== 'create' ? organization.data : null}
    />
  ) : null;
};
