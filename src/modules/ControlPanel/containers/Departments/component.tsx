import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/Department/requests';
import { organizationDepartmentsAtom } from '../../../../store/modules/Organization/atoms/organizationDepartmentsAtom';
import { Departments } from '../../pages/Departments';

export const DepartmentsContainer: FC = () => {
  const [departments, { get: getDepartments }] = useAtom(organizationDepartmentsAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getDepartments(activeWorkspace);
    }
  }, [activeWorkspace, getDepartments, token]);

  useEffect(() => {
    setLoading(departments.loading);

    if (!departments.loading && departments.error) {
      throw departments.error;
    }
  }, [departments, setLoading]);

  const handleCreate = (data: any) => {
    setLoading(true);
    Requests.create(data)
      .then(() => {
        getDepartments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  };

  const handleUpdate = (id: string, data: any) => {
    setLoading(true);
    Requests.update(id, data)
      .then(() => {
        getDepartments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  };

  const handleDelete = (id: string) => () => {
    setLoading(true);
    Requests.delete(id)
      .then(() => {
        getDepartments(activeWorkspace);
      })
      .finally(() => setLoading(false));
  }


  const loadedSuccessfully = !departments.loading && !departments.error;

  return loadedSuccessfully ? (
    <Departments
      departments={departments.data}
      handleDelete={handleDelete}
      handleCreate={handleCreate}
      handleUpdate={handleUpdate}
    />
  ) : null;
}
