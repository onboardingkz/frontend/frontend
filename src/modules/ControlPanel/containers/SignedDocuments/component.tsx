import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom, organizationSignedDocumentsAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/SignedDocument/requests';
import { message } from 'antd';
import { SignedDocuments } from '../../pages/SignedDocuments';

export const SignedDocumentsContainer: FC = () => {
  const [documents, { get: getDocuments }] = useAtom(organizationSignedDocumentsAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getDocuments(activeWorkspace);
    }
  }, [activeWorkspace, getDocuments, token]);

  useEffect(() => {
    setLoading(documents.loading);

    if (!documents.loading && documents.error) {
      throw documents.error;
    }
  }, [documents, setLoading]);

  const handleDelete = (id: string) => () => {
    Requests.delete(id)
      .then(async () => {
        await getDocuments(activeWorkspace);
        await message.success('Successfully deleted');
      })
      .catch(async () => {
        await message.error('Something happened');
      })
      .finally(() => setLoading(false));
  }

  const loadedSuccessfully = !documents.loading && !documents.error;

  return loadedSuccessfully ? (
    <SignedDocuments
      documents={documents.data || []}
      handleDelete={handleDelete}
    />
  ) : null;
}
