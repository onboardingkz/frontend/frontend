import {
  WorkspaceContainer,
} from './containers';

export const Routes = [
  {
    path: '/',
    component: WorkspaceContainer
  }
];

