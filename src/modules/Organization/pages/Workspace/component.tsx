import React, { FC } from 'react';
import { Props } from './props';
import { Container } from '../../../../components/Container';
import {
  Avatar,
  Button,
  Tooltip,
  Typography,
} from 'antd';
import { AppleOutlined, SettingOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { getAvatar } from '../../../../core/utils';
import { blue } from '@ant-design/colors';
import { ObjectTable } from '../../../../components/ObjectTable';
import { useAtom } from '@reatom/react';
import { activeWorkspaceAtom } from '../../../../store/modules/Organization/atoms';

export const Workspace: FC<Props> = ({ organization, isOwner, ...rest }: Props) => {

  const router = useHistory();

  const columns = {
    bin: 'BIN',
    address: 'Address',
    email: 'Email',
    phone: 'Phone'
  };

  const [activeWorkspace] = useAtom(activeWorkspaceAtom);

  return activeWorkspace ? (
    <div className={'animate__animated animate__fadeIn'} style={{ marginTop: 100, height: '100%' }} {...rest}>
      <Container>
        <div className={'d-flex align-items-start'} style={{ gap: 40, flex: 1 }}>
          <div>
            <Avatar src={getAvatar(organization?.avatar)} size={120} style={{ backgroundColor: blue.primary }} />
          </div>
          <div style={{ flex: 1 }}>
            {organization ? (
              <>
                <Typography.Title className={'d-flex align-items-center'} style={{ gap: 15 }} level={3}>
                  <span>{organization.name}</span>
                  {isOwner ? (
                    <Tooltip title={'Control panel'}>
                      <Button onClick={() => router.push('/control-panel')}>
                        <SettingOutlined />
                      </Button>
                    </Tooltip>
                  ) : null}

                </Typography.Title>
                <ObjectTable
                  columns={columns}
                  item={organization}
                />
              </>
            ) : null}
          </div>
        </div>
      </Container>
    </div>
  ) : (
    <div style={{ paddingTop: 300, height: '100%' }} {...rest}>
      <Container size={500}>
        <div className="d-flex" style={{ gap: 20 }}>
          <div><AppleOutlined style={{ fontSize: 50, marginTop: 20, color: blue.primary }} /></div>
          <div>
            <Typography.Title className={'text-center'} level={2}>Choose your workspace on the top-right of the page</Typography.Title>
            <Typography.Title className={'text-center'} level={5}>If you don't have one - you can create</Typography.Title>
          </div>
        </div>
      </Container>
    </div>
  );
};
