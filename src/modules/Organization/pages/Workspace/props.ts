import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly organization: any;
  readonly isOwner?: boolean;
};
