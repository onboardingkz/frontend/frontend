import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import {
  organizationAtom,
} from '../../../../store/modules/Organization/atoms';
import { Workspace } from '../../pages/Workspace';
import { authUserAtom } from '../../../../store/modules/Auth/atoms';
import { isOwner } from '../../../../core/utils';

export const WorkspaceContainer: FC = () => {

  const [loading, { set: setLoading }] = useAtom(loadingAtom);
  const [organization] = useAtom(organizationAtom);
  const [user] = useAtom(authUserAtom);

  useEffect(() => {
    setLoading(organization.loading);

    if (!organization.loading && organization.error) {
      throw organization.error;
    }
  }, [organization, setLoading]);

  return !loading ? (
    <Workspace organization={organization.data} isOwner={isOwner(organization, user)} />
  ) : null;
};
