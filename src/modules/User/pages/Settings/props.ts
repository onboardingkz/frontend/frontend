import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly handleUpdate: (data: any) => void;
  readonly handleUpload: (data: any, dataFields: any) => void;
  readonly handleSignatureUpload: (data: any, dataFields: any) => void;
};
