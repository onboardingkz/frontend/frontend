import React, { FC, useEffect, useRef, useState } from 'react';
import { Props } from './props';
import { Container } from '../../../../components/Container';
import { Avatar, Button, Form, Input, Modal, Typography } from 'antd';
import { useAtom } from '@reatom/react';
import { authUserAtom } from '../../../../store/modules/Auth/atoms';
import { getAvatar, urlToFile } from '../../../../core/utils';
import hexToRgba from 'hex-to-rgba';
import { blue, grey, orange } from '@ant-design/colors';
import { CameraOutlined } from '@ant-design/icons';
import CanvasDraw from 'react-canvas-draw';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';

export const Settings: FC<Props> = ({ handleUpdate, handleUpload, handleSignatureUpload, ...rest }: Props) => {
  const [user] = useAtom(authUserAtom);

  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [signatureModalOpen, setSignatureModalOpen] = useState(false);

  const [, setAvatar] = useState<any>(null);

  const avatarInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const signatureCanvasRef = useRef() as React.MutableRefObject<any>;


  const data = {
    ...(user.data.name !== name ? {name} : {}),
    ...(user.data.lastName !== lastName ? {lastName} : {}),
    ...(user.data.phone !== phone ? {phone} : {}),
    ...(user.data.email !== email ? {email} : {}),
  };

  useEffect(() => {
    if (user.data?._id) {
      setName(user.data.name);
      setLastName(user.data.lastName);
      setPhone(user.data.phone);
      setEmail(user.data.email);
    }
  }, [user]);

  const onClick = () => {
    const form = new FormData();
    if (avatarInputRef.current.files?.length) {
      const avatarFile = avatarInputRef.current.files[0];
      form.append('file', avatarFile);
      avatarInputRef.current.value = '';
      handleUpload(form, data);
    } else {
      handleUpdate(data);
    }
  }

  const handleSignatureUpdate = () => {
    const signatureData = signatureCanvasRef?.current?.canvasContainer.childNodes[1].toDataURL("image/png").replace("image/png", "image/octet-stream");
    urlToFile(signatureData, `signature_${user.data?.name}.png`, 'image/png')
      .then(file => {
        const form = new FormData();
        form.append('file', file);
        handleSignatureUpload(form, {});
        setSignatureModalOpen(false);
      });
  }

  return (
    <div className={'animate__animated animate__fadeIn'} style={{ paddingTop: 80, height: '100%' }} {...rest}>
      <Container size={500}>
        <Typography.Title className={'text-center mb-2'} level={2}>Profile settings</Typography.Title>
        <Form layout="vertical">
          <Form.Item className={'text-center mt-5'}>
            <label style={{
              position: 'relative',
              display: 'inline-block',
              borderRadius: '100%',
              overflow: 'hidden',
            }}>
              <Avatar src={avatarInputRef.current?.files?.[0] ? URL.createObjectURL(avatarInputRef.current?.files?.[0]) : getAvatar(user.data?.avatar)} size={120} style={{ backgroundColor: blue.primary }} />
              <div css={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                backgroundColor: hexToRgba('#000', 0.4),
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-around',
                color: '#fff',
                fontSize: 25,
                cursor: 'pointer',
                opacity: 0,
                transition: 'opacity 0.3s',
                "&:hover": {
                  opacity: 1
                }
              }}>
                <CameraOutlined />
                <input type={'file'} accept="image/*" hidden ref={avatarInputRef} onChange={(e) => setAvatar(e.currentTarget?.files?.[0])} />
              </div>
            </label>
          </Form.Item>
          <Form.Item label={"First name"} required>
            <Input value={name} size={'middle'} placeholder={'Moldir'} type={"text"} onChange={(e) => setName(e.currentTarget.value)} />
          </Form.Item>
          <Form.Item label={"Last name"} required>
            <Input value={lastName} size={'middle'} placeholder={'Dybyspaeva'} type={"text"} onChange={(e) => setLastName(e.currentTarget.value)} />
          </Form.Item>
          <Form.Item label={"Phone number"} required>
            <Input value={phone} size={'middle'} placeholder={'+77071234567'} type={"text"} onChange={(e) => setPhone(e.currentTarget.value)} />
          </Form.Item>
          <Form.Item label={"Email address"} required>
            <Input
              value={email}
              size={'middle'}
              type={"text"}
              placeholder={"example@example.com"}
              onChange={(e) => setEmail(e.currentTarget.value)}
            />
          </Form.Item>
        </Form>
        {user.data?.signature ? (
          <div className="text-center d-flex align-items-center justify-content-between" style={{ gap: 20 }}>
            <Typography.Title level={4}>Signature</Typography.Title>
            <img
              src={getAvatar(user.data?.signature)}
              onClick={() => setSignatureModalOpen(true)}
              css={{
                width: 100,
                height: 50,
                borderRadius: 10,
                background: hexToRgba(grey.primary || 'grey', .2),
                cursor: 'pointer',
                transition: 'background 0.2s',
                '&:hover': {
                  background: hexToRgba(orange.primary || 'orange', .2)
                }
              }}
              alt={`${user.data?.name} signature`}
            />
          </div>
        ) : null}
        {user.data?.signature ? null : (
          <Button
            className={'animate__animated animate__lightSpeedInRight mt-3'}
            size={"large"}
            block
            onClick={() => setSignatureModalOpen(true)}
          >
            Add signature
          </Button>
        )}

        {signatureModalOpen ? (
          <Modal title={'Signature'} visible={signatureModalOpen} onOk={handleSignatureUpdate} onCancel={() => setSignatureModalOpen(false)}>
            <CanvasDraw
              ref={signatureCanvasRef}
              brushColor={'blue'}
              brushRadius={2}
              lazyRadius={0}
              canvasWidth={400}
              canvasHeight={200}
              style={{
                border: `2px solid ${blue.primary}`,
                borderRadius: 10,
                margin: 'auto'
              }}
            />
          </Modal>
        ) : null}


        <Button
          className={'animate__animated animate__lightSpeedInRight mt-3'}
          size={"large"}
          type={"primary"}
          block
          onClick={onClick}
        >
          Save!
        </Button>

      </Container>
    </div>
  );
};
