import React, { FC, useState } from 'react';
import { Props } from './props';
import { useHistory } from 'react-router-dom';
import { Container } from '../../../../components/Container';
import { Button, Divider, Form, Input, Typography } from 'antd';

export const Login: FC<Props> = ({ handleLogin, ...rest }: Props) => {
  const router = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const data = {
    email,
    password
  };

  return (
    <div className={'animate__animated animate__fadeIn'} style={{ paddingTop: 300 }} {...rest}>
      <Container size={500}>
        <Typography.Title className={'text-center'} level={2}>Sign in</Typography.Title>
        <Divider orientation={"center"} plain>Welcome to Honeycomb</Divider>
        <Form.Item>
          <Input size={'middle'} type={"text"} placeholder={"Type your email"} onChange={(e) => setEmail(e.currentTarget.value)} />
        </Form.Item>
        <Form.Item>
          <Input.Password size={'middle'} placeholder={"Type your password"} onChange={(e) => setPassword(e.currentTarget.value)} />
        </Form.Item>
        <Button className={'animate__animated animate__lightSpeedInRight'} size={"large"} type={"primary"} block onClick={() => handleLogin(data)}>Sign in!</Button>
        <Typography.Paragraph className={'mt-3 text-center'}>
          Don't have an account?
          <Button type={"link"} className={'p-0 ml-2'} onClick={() => router.push('/auth/register')}>Create</Button>
        </Typography.Paragraph>
      </Container>
    </div>
  );
};
