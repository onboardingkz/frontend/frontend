import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly notifications: ReadonlyArray<any>;
  readonly notificationOpened: () => void;
}
