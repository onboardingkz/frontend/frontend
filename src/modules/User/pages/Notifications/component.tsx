import React, { FC, useState } from 'react';
import { Props } from './props';
import { useDebouncedCallback } from 'use-debounce';
import { Container } from '../../../../components/Container';
import { Badge, Card, Typography } from 'antd';
import { notificationCategories } from '../../../../core/mappings';
import hexToRgba from 'hex-to-rgba';
import { blue, orange } from '@ant-design/colors';

export const Notifications: FC<Props> = ({ notifications, notificationOpened, ...rest }: Props) => {

  const notificationsNew = notifications.filter(n => !n?.read);

  const [active, setActive] = useState<any>(null);

  const onMouseMove = useDebouncedCallback(
    () => {
      if (notificationsNew.length) {
        notificationOpened();
      }
    },
    300);

  return (
    <div style={{ flex: 1, overflow: 'auto', height: '100%' }} onMouseMove={onMouseMove} {...rest}>
      <Container size={'100%'} style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
        <Typography.Title level={2} className={'m-0 my-3'}>Notifications</Typography.Title>
        <div className={'d-flex mb-3'} style={{ gap: 10, height: '100%', overflow: 'auto' }}>
          <div style={{ flex: !active ? 1 : '0 0 300px' }}>
            {notifications.map((n: any, i) => (
              <div className={'mb-2 animate__animated animate__lightSpeedInLeft'} key={i}>
                <Badge className={'d-block'} count={!n.read ? 'New' : 0} size={'default'}>
                  <Card
                    size={'small'}
                    css={{
                      backgroundColor: !n.read ? hexToRgba(orange.primary || 'grey', 0.1) : '#fff',
                      cursor: 'pointer',
                      transition: 'transform 0.2s, background 0.2s',
                      '&:hover': {
                        transform: 'scale(0.99)',
                        background: hexToRgba(blue.primary || 'blue', 0.05)
                      }
                    }}
                    onClick={() => setActive(n)}
                  >
                    <Typography.Title level={5} className={'m-0 mb-1'}>{n?.title || 'Empty notification'}</Typography.Title>
                    <Typography.Paragraph className={'m-0'}>from: {notificationCategories[n?.category || 'empty']}</Typography.Paragraph>
                  </Card>
                </Badge>
              </div>
            ))}
          </div>
          {active ? (
            <div style={{
              flex: 1,
            }}>
              <Card style={{
                position: 'sticky',
                top: 0,
              }}>
                <Typography.Title level={4} className={'m-0 mb-1'}>{active?.title || 'Empty notification'}</Typography.Title>
                <Typography.Paragraph className={'m-0'}>from: {notificationCategories[active?.category || 'empty']}</Typography.Paragraph>
                <Typography.Paragraph className={'m-0'}>{active?.createdAt}</Typography.Paragraph>
              </Card>
            </div>
          ) : null}
        </div>

      </Container>

    </div>
  )
}
