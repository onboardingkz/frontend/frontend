import React, { FC, useState } from 'react';
import { Props } from './props';
import { useHistory } from 'react-router-dom';
import { Container } from '../../../../components/Container';
import { Alert, Button, Checkbox, Form, Input, Progress, Steps, Typography } from 'antd';

export const Registration: FC<Props> = ({ handleRegister, ...rest }: Props) => {
  const router = useHistory();

  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [policy, setPolicy] = useState(false);

  const data = {
    name,
    lastName,
    email,
    phone,
    password
  };

  const currentStepProgress = () => {
    const privateDataValidation = [name.length > 0, lastName.length > 0, phone.length > 0];
    const credentialsDataValidation = [email.length > 0, password.length > 6];

    const privateDataValidationPassed = privateDataValidation.filter(n => n);
    const credentialsDataValidationPassed = credentialsDataValidation.filter(n => n);

    if (credentialsDataValidationPassed.length === credentialsDataValidation.length && privateDataValidationPassed.length === privateDataValidation.length) {
      return [3, 100];
    }
    const currentStep = privateDataValidation.length === privateDataValidationPassed.length ? 1 : 0;
    return [currentStep, (currentStep ? (credentialsDataValidationPassed.length/credentialsDataValidation.length) : (privateDataValidationPassed.length/privateDataValidation.length)) * 100];
  };

  const [currentStep, currentProgress] = currentStepProgress();

  return (
    <div className={'animate__animated animate__fadeIn'} style={{ paddingTop: 200 }} {...rest}>
      <Container size={500}>
        <Typography.Title className={'text-center mb-2'} level={2}>Create account</Typography.Title>
        <Typography.Paragraph className={'mt-0 text-center mb-4'}>
          Already have an account?
          <Button type={"link"} className={'p-0 ml-2'} onClick={() => router.push('/auth')}>Sign in</Button>
        </Typography.Paragraph>
        <Steps direction={'vertical'} current={currentStep} percent={currentProgress}>
          <Steps.Step title="Personal information" description={
            <>
              <Alert
                className={'my-4'}
                showIcon
                message={"We don't share your personal information"}
                type={"success"}
              />
              <Form layout="vertical">
                <Form.Item label={"First name"} required>
                  <Input size={'middle'} placeholder={'Moldir'} type={"text"} onChange={(e) => setName(e.currentTarget.value)} />
                </Form.Item>
                <Form.Item label={"Last name"} required>
                  <Input size={'middle'} placeholder={'Dybyspaeva'} type={"text"} onChange={(e) => setLastName(e.currentTarget.value)} />
                </Form.Item>
                <Form.Item label={"Phone number"} required>
                  <Input size={'middle'} placeholder={'+77071234567'} type={"text"} onChange={(e) => setPhone(e.currentTarget.value)} />
                </Form.Item>
              </Form>
            </>
          } />
          <Steps.Step title="Your credentials" description={currentStep ? (
            <div className={'animate__animated animate__lightSpeedInRight'}>
              <Alert
                className={'my-4'}
                message={"Will be used to sign in to your account"}
                type={"info"}
              />
              <Form layout={"vertical"}>
                <Form.Item label={"Email address"} required>
                  <Input
                    size={'middle'}
                    type={"text"}
                    placeholder={"example@example.com"}
                    onChange={(e) => setEmail(e.currentTarget.value)}
                  />
                </Form.Item>
                <Form.Item label={'Password'}>
                  <Input.Password
                    size={'middle'}
                    onChange={(e) => setPassword(e.currentTarget.value)}
                  />
                  {password.length ? (
                    <Progress
                      className={'mt-3'}
                      percent={password.length > 6 ? (password.length > 10 ? 100 : 65) : (20)}
                      size={'small'}
                      status={password.length <= 6 ? "exception" : undefined}
                      showInfo={false}
                    />
                  ) : null}
                </Form.Item>
              </Form>

              <label>
                <Typography.Paragraph>
                  <Checkbox className={'mr-2'} onChange={e => setPolicy(e.target.checked)} />
                  <span>I agree with terms and conditions</span>
                </Typography.Paragraph>
              </label>
              {currentStep > 1 ? (
                <>
                  <Button
                    className={'animate__animated animate__lightSpeedInRight mt-3'}
                    size={"large"}
                    type={"primary"}
                    block
                    onClick={() => handleRegister(data)}
                    disabled={!(policy && currentStep > 1)}
                  >
                    Sign up!
                  </Button>
                </>
              ) : null}
            </div>
          ) : null} />
        </Steps>
      </Container>
    </div>
  );
};
