import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { useHistory } from 'react-router-dom';

export const LogoutContainer: FC = () => {
  const [, { clear }] = useAtom(authAtom);
  const router = useHistory();
  useEffect(() => {
    if (localStorage) {
      clear();
      localStorage.removeItem('authToken');
      localStorage.removeItem('workspaceId');
      // eslint-disable-next-line no-restricted-globals
      location.replace('/');
    }
  }, [clear, router]);
  return (
    <></>
  );
};
