import React, { FC } from 'react';
import { authStore } from '../../../../store';
import { Login } from '../../pages/Login';
import { message } from 'antd';
import { useAtom } from '@reatom/react';
import { useHistory } from 'react-router-dom';

export const LoginContainer: FC = () => {
  const [token] = useAtom(authStore.atoms.authAtom);
  const router = useHistory();

  if (token) {
    router.push('/organization');
  }

  const handleLogin = (data: any) => {
    authStore.requests.Requests.login(data)
      .then(res => {
        const { token: tokenRes } = res.data;
        if (localStorage) {
          localStorage.setItem('authToken', tokenRes);
          // eslint-disable-next-line no-restricted-globals
          location.replace('/organization');
        }
      })
      .catch(async (err) => {
        if (err?.response) {
          await message.error(err.response?.data?.message);
        }
      });
  };

  return (
    <Login handleLogin={handleLogin} />
  );
};
