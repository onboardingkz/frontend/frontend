import { message } from 'antd';
import React, { FC, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { emailStore } from '../../../../store';

export const ConfirmEmailContainer: FC = () => {
  const { search } = useLocation();
  const router = useHistory();

  useEffect(() => {
    const query = new URLSearchParams(search);

    const token = query?.get('token');
    if (token) {
      emailStore.requests.Requests.confirm(token)
        .then(async () => {
          await message.success("Your email was successfully confirmed");
        })
        .finally(() => {
          router.push('/');
        });
    }
  }, [router, search]);

  return (
    <></>
  );
};
