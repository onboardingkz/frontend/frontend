import React, { FC } from 'react';
import { authStore } from '../../../../store';
import { Registration } from '../../pages/Registration';
import { message } from 'antd';
import { useAtom } from '@reatom/react';
import { useHistory } from 'react-router-dom';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
export const RegistrationContainer: FC = () => {
  const [token] = useAtom(authStore.atoms.authAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);
  const router = useHistory();

  if (token) {
    router.push('/');
  }

  const handleRegister = (data: any) => {
    setLoading(true);
    authStore.requests.Requests.register(data)
      .then(res => {
        const { token: tokenRes } = res.data.session;
        if (localStorage) {
          localStorage.setItem('authToken', tokenRes);
          // eslint-disable-next-line no-restricted-globals
          location.replace('/organization');
        }
      })
      .catch(async (err) => {
        if (err?.response) {
          await message.error(err.response?.data?.message);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Registration handleRegister={handleRegister} />
  );
};
