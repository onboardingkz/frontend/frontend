import React, { FC } from 'react';
import { message } from 'antd';
import { useAtom } from '@reatom/react';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { Requests } from '../../../../store/modules/Auth/requests';
import { Requests as CdnRequests } from '../../../../store/modules/CDN/requests';
import { Settings } from '../../pages/Settings';
import { authUserAtom } from '../../../../store/modules/Auth/atoms';
export const SettingsContainer: FC = () => {
  const [, { set: setLoading }] = useAtom(loadingAtom);
  const [, { get: getUser }] = useAtom(authUserAtom);

  const handleUpdate = (data: any) => {
    setLoading(true);
    Requests.update(data)
      .then(() => {
        getUser();
        message.success('Updated successfully');
      })
      .catch((error) => {
        if (error.response.status === 405){
          message.error('User with this email already exists!');
        }
        else message.error('Error');
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleUpload = (data: any, dataFields: any) => {
    setLoading(true);
    CdnRequests.upload(data, 'avatars')
      .then((res) => {
        handleUpdate({ ...dataFields, avatar: res.data });
      })
      .finally(() => setLoading(false));
  }

  const handleSignatureUpload = (data: any, dataFields: any) => {
    setLoading(true);
    CdnRequests.upload(data, 'signatures')
      .then((res) => {
        handleUpdate({ ...dataFields, signature: res.data });
      })
      .finally(() => setLoading(false));
  }

  return (
    <Settings handleUpdate={handleUpdate} handleUpload={handleUpload} handleSignatureUpload={handleSignatureUpload} />
  );
};
