import React, { FC, useContext, useEffect, useState } from 'react';
import { userStore } from '../../../../store';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { message } from 'antd';
import { Notifications, NotificationsProps } from '../../pages/Notifications';
import { SocketConnection } from '../../../../components/SocketConnection';

export const NotificationsContainer: FC = () => {
  const [notifications, setNotifications] = useState<NotificationsProps['notifications']>([]);
  const [token] = useAtom(authAtom);
  const socket = useContext(SocketConnection);
  const [loading, setLoading] = useState(false);

  const fetchNotifications = () => {
    userStore.requests.Requests.notificationsAll()
      .then(res => {
        setNotifications(res.data);
      })
      .catch(async () => {
        await message.error('Server error');
      });
  }

  const notificationsOpened = () => {
    if (!loading) {
      setLoading(true);
      userStore.requests.Requests.notificationsOpened()
        .then(res => {
          setNotifications(res.data);
        })
        .finally(() => {
          setLoading(false);
        });
    }

  }

  useEffect(() => {
    if (token) {
      fetchNotifications();
    }
  }, [token]);

  useEffect(() => {
    socket?.on('NOTIFY', (notification) => {
      setNotifications(prev => [notification, ...prev]);
    });
  }, [socket]);

  return <Notifications notifications={notifications} notificationOpened={notificationsOpened} />;
};
