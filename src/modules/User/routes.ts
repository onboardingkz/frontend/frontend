import {
  RegistrationContainer,
  LogoutContainer,
  LoginContainer,
  NotificationsContainer, SettingsContainer,
} from './containers';
import { ConfirmEmailContainer } from './containers/ConfirmEmail';

export const Routes = [
  {
    path: '/register',
    component: RegistrationContainer,
    noAuth: true
  },
  {
    path: '/',
    component: LoginContainer,
    noAuth: true
  },
  {
    path: '/logout',
    component: LogoutContainer,
    noAuth: true
  },
  {
    path: '/notifications',
    component: NotificationsContainer,
    fullHeight: true
  },
  {
    path: '/confirm',
    component: ConfirmEmailContainer
  },
  {
    path: '/settings',
    component: SettingsContainer,
    fullHeight: true
  }
];
