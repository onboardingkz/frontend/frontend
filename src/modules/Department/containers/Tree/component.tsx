import React, { FC, useEffect } from 'react';
import { Tree } from '../../pages/Tree';
import { useAtom } from '@reatom/react';
import {
  activeWorkspaceAtom,
} from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';
import { organizationDepartmentsAtom } from '../../../../store/modules/Organization/atoms/organizationDepartmentsAtom';

export const TreeContainer: FC = () => {

  const [departments, { get: getDepartments }] = useAtom(organizationDepartmentsAtom);
  const [token] = useAtom(authAtom);
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [, { set: setLoading }] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      getDepartments(activeWorkspace);
    }
  }, [activeWorkspace, getDepartments, token]);

  useEffect(() => {
    setLoading(departments.loading);

    if (!departments.loading && departments.error) {
      throw departments.error;
    }
  }, [departments, setLoading]);

  const loadedSuccessfully = !departments.loading && !departments.error;

  return loadedSuccessfully ? (
    <Tree departments={departments.data} />
  ) : null;
}
