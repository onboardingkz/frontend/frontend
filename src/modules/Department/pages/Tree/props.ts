import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  departments: ReadonlyArray<any>;
  frame?: boolean;
  additionalColumns?: ReadonlyArray<Record<any, any>>;
}
