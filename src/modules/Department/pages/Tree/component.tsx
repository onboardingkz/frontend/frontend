import React, { FC } from "react";
import { Props } from './props';

import RenderTree from 'react-d3-tree';
import { useAtom } from '@reatom/react';
import { organizationAtom } from '../../../../store/modules/Organization/atoms';

export const Tree: FC<Props> = ({ departments, frame, additionalColumns = [], ...rest }: Props) => {
  const [organization] = useAtom(organizationAtom);

  // @ts-ignore
  const recurseMapFilter = (departments: ReadonlyArray<any>) => {
    return departments?.map(n => ({
      name: n.title,
      ...(n.children ? { children: recurseMapFilter(n.children) } : {})
    }));
  }

  const data = { name: organization.data?.name, children: recurseMapFilter(departments) };

  return (
    <div className={'animate__animated animate__fadeIn h-100'} {...rest}>
      {departments?.length ? (
        <div style={{ height: '100%', backgroundColor: '#f2fcff' }}>
          <RenderTree
            data={data}
            nodeSize={{ x: 200, y: 200 }}
            orientation={'vertical'}
            rootNodeClassName="node__root animate__animated animate__bounceInDown animate__slow"
            branchNodeClassName="node__branch animate__animated animate__fadeIn animate__slower"
            leafNodeClassName="node__leaf animate__animated animate__fadeIn animate__slower"
            zoomable
            pathClassFunc={() => 'node__link animate__animated animate__fadeIn slower animate__delay-1s'}
            svgClassName={'tree-root'}
            translate={{ x: window.innerWidth / 2, y: 200 }}
            transitionDuration={300}
          />
        </div>

      ) : null}
    </div>
  );
}
