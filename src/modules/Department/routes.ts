import { TreeContainer } from './containers/Tree';

export const Routes = [
  {
    path: '/',
    component: TreeContainer,
    fullHeight: true
  },
];
