import React, { FC } from 'react';
import { Props } from './props';
import { Container } from '../../../../components/Container';
import { Typography } from 'antd';

export const Error404: FC<Props> = ({ ...rest }: Props) => {
  return (
    <div style={{ paddingTop: 300 }} {...rest}>
      <Container size={500}>
        <h1 className={'text-center'} style={{
          fontSize: 70,
          margin: 0
        }}>404</h1>
        <Typography.Title className={'text-center'} level={5}>The page you're looking for doesn't exist</Typography.Title>
      </Container>
    </div>
  );
};
