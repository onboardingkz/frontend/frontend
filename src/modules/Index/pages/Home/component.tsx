import React, { FC } from 'react';
import { Props } from './props';
import {
  DepartmentsSection,
  FeaturesSection,
  HeaderSection, OfficeMapSection,
  RulesSection,
  SignDocumentSection,
} from './sections';
import { Footer } from '../../../../components/Footer';
import { BackTop } from 'antd';
import { UpOutlined } from '@ant-design/icons';
import hexToRgba from 'hex-to-rgba';
import { blue } from '@ant-design/colors';

export const Home: FC<Props> = ({ ...rest }: Props) => {
  return (
    <div style={{
      fontFamily: "Gotham Rounded",
      height: '100%'
    }} {...rest}>
      <HeaderSection />
      <FeaturesSection />
      <SignDocumentSection />
      <RulesSection />
      <DepartmentsSection />
      <OfficeMapSection />

      <BackTop>
        <div css={{
          background: blue.primary,
          color: '#fff',
          width: '100%',
          height: '100%',
          display: "flex",
          alignItems: 'center',
          justifyContent: 'space-around',
          borderRadius: 10,
          textAlign: 'center',
          padding: 0,
          transition: 'transform 0.1s, box-shadow 0.2s',
          '&:hover': {
            transform: 'scale(1.1)',
            boxShadow: `0 0 3px 6px ${hexToRgba(blue.primary || 'blue', .2)}`
          }
        }}>
          <UpOutlined />
        </div>
      </BackTop>
      <Footer fullHeight={true} />
    </div>
  );
};
