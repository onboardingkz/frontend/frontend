import React, { FC } from 'react';
import { Container } from '../../../../../components/Container';
import { Typography } from 'antd';
import { BuildOutlined } from '@ant-design/icons';
import { orange } from '@ant-design/colors';
// @ts-ignore
import ReactTypingEffect from 'react-typing-effect';

export const OfficeMap: FC = () => {

  return (
    <div className={'py-5'}>
      <Container>
        <div className="d-flex">
          <div className={'mr-5'}>
            <BuildOutlined spin style={{ fontSize: 70, color: orange.primary }} />
          </div>
          <div>
            <Typography.Title level={2} style={{ fontWeight: 500 }}><ReactTypingEffect text={["Office Map."]} speed={300} typingDelay={100}/></Typography.Title>
            <Typography.Paragraph style={{ fontSize: 17 }}>
              When a new employee comes in, the recruiter has to take a mini tour of the office, and it's not always easy for the employee to fully remember the location of each of the sections of the office, especially if your office is huge. We at Onboarding solved the problem with a public interactive office map. The employee will be able to find the required part of the office at any time
            </Typography.Paragraph>
          </div>
        </div>

      </Container>
    </div>
  );
}
