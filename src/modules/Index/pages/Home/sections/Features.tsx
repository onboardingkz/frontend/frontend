import React, { FC } from 'react';
import { Container } from '../../../../../components/Container';
import { Typography } from 'antd';
import { FontColorsOutlined } from '@ant-design/icons';
// @ts-ignore
import ReactTypingEffect from 'react-typing-effect';

export const Features: FC = () => {

  return (
    <div className={'py-5'}>
      <Container>
        <div className="d-flex">
          <div className={'mr-5'}>
            <FontColorsOutlined spin style={{ fontSize: 70 }} />
          </div>
          <div>
            <Typography.Title level={2} style={{ fontWeight: 500 }}><ReactTypingEffect text={["About us."]} speed={300} typingDelay={100}/></Typography.Title>
            <Typography.Paragraph style={{ fontSize: 17 }}>
              The main mission of our platform is to solve a number of inconveniences in the recruitment and subsequent work process of a new employee. Automation of the "onboarding" process, solving everyday tasks like navigating around the office, signing documents, and so on. Our goal is to fully automate and shorten as much as possible the process of putting a new employee into work.
            </Typography.Paragraph>
          </div>
        </div>

      </Container>
    </div>
  );
}
