import React, { FC } from 'react';
import { Container } from '../../../../../components/Container';
import { Typography } from 'antd';
import { ForkOutlined } from '@ant-design/icons';
import { cyan } from '@ant-design/colors';
// @ts-ignore
import ReactTypingEffect from 'react-typing-effect';

export const Departments: FC = () => {

  return (
    <div className={'py-5'}>
      <Container>
        <div className="d-flex">
          <div className={'mr-5'}>
            <ForkOutlined spin style={{ fontSize: 70, color: cyan.primary }} />
          </div>
          <div>
            <Typography.Title level={2} style={{ fontWeight: 500 }}><ReactTypingEffect text={["Company tree."]} speed={300} typingDelay={100}/></Typography.Title>
            <Typography.Paragraph style={{ fontSize: 17 }}>
              It so happens that a company can consist of a very large number of departments that carry out their part of the mission. And the main problem of this is communication between departments. In order for an employee to understand which employee to contact, he has to go through a number of inconveniences. Starting with finding out what a particular department does, ending with manually searching for the right person in the contact book. Onboarding provides convenient viewing, search and description of absolutely the entire structure of the company.
            </Typography.Paragraph>
          </div>
        </div>

      </Container>
    </div>
  );
}
