import React, { FC } from 'react';
import { Container } from '../../../../../components/Container';
import { Typography } from 'antd';
import { FilePdfOutlined } from '@ant-design/icons';
import { red } from '@ant-design/colors';
// @ts-ignore
import ReactTypingEffect from 'react-typing-effect';

export const SignDocument: FC = () => {

  return (
    <div className={'py-5'}>
      <Container>
        <div className="d-flex">
          <div className={'mr-5'}>
            <FilePdfOutlined spin style={{ fontSize: 70, color: red.primary }} />
          </div>
          <div>
            <Typography.Title level={2} style={{ fontWeight: 500 }}><ReactTypingEffect text={["Sign documents easily."]} speed={300} typingDelay={100}/></Typography.Title>
            <Typography.Paragraph style={{ fontSize: 17 }}>
              Document management has never been so easy. Now you do not need to manually print out the document that you need to sign, sign, scan and send to your HR department by e-mail. Simply find the document you need to sign in the company's list of public documents and click Sign. That's it!
            </Typography.Paragraph>
          </div>
        </div>
      </Container>
    </div>
  );
}
