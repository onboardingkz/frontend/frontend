import React, { FC } from 'react';
import { Container } from '../../../../../components/Container';
import { Typography } from 'antd';
import { OrderedListOutlined } from '@ant-design/icons';
import { magenta } from '@ant-design/colors';
// @ts-ignore
import ReactTypingEffect from 'react-typing-effect';

export const Rules: FC = () => {

  return (
    <div className={'py-5'}>
      <Container>
        <div className="d-flex">
          <div className={'mr-5'}>
            <OrderedListOutlined spin style={{ fontSize: 70, color: magenta.primary }} />
          </div>
          <div>
            <Typography.Title level={2} style={{ fontWeight: 500 }}><ReactTypingEffect text={["Office rules."]} speed={300} typingDelay={100}/></Typography.Title>
            <Typography.Paragraph style={{ fontSize: 17 }}>
              Each office has its own list of rules of conduct. Do not eat at the table, do not make noise, and so on. The main disadvantage of the status quo is that office rules are simply hung up and everyone forgets about it. And if you need to add or remove one or more rules, you have to reprint the set of rules and manually notify employees about the changes. "Onboarding" provides a single source of truth for rules and notifies all employees of changes. And even a child understands how to work with this.
            </Typography.Paragraph>
          </div>
        </div>
      </Container>
    </div>
  );
}
