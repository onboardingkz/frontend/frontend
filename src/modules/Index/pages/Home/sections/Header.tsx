import React, { FC } from 'react';
import bg from '../../../../../assets/images/landing-bg.svg';
import pattern from '../../../../../assets/images/pattern.svg';
import { Button, Typography } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { magenta, purple } from '@ant-design/colors';
// @ts-ignore
import ReactTypingEffect from 'react-typing-effect';

export const Header: FC = () => {
  const router = useHistory();

  return (
    <div
      className={'py-5'}
      style={{
        backgroundImage: `linear-gradient(to right, rgb(25 144 255 / 6%), rgb(255 255 255 / 27%)), url(${pattern}), url(${bg})`,
        backgroundSize: 'cover, auto, cover',
        textAlign: 'center',
      }}
    >

      <Typography.Title
        className={'text-center'}
        style={{
          fontSize: 40,
          fontWeight: 500,
          marginTop: 150,
          marginBottom: 5
        }}
      >
        <span style={{ background: magenta.primary, padding: 10, color: '#fff' }}>&#8250; <ReactTypingEffect text={["Honeycomb."]} speed={300} typingDelay={100}/></span>
      </Typography.Title>
      <Typography.Paragraph
        className={'m-0 mt-2'}
        style={{
          fontSize: 17,
          maxWidth: 500,
          display: 'inline-block',
        }}
      >
        <span style={{ background: purple.primary, padding: 10, color: '#fff' }}>OnBoarding platform, that helps your new employee get into your company</span>
      </Typography.Paragraph>

      <div className={'my-5'}>
        <Button
          type={'primary'}
          size={'large'}
          shape={'round'}
          className={'mr-2'}
          style={{
            fontSize: 14
          }}
          onClick={() => router.push('/auth/register')}
        >
          <UserOutlined />
          <span>Create account</span>
        </Button>
        <Button
          shape={'round'}
          size={'large'}
          style={{
            fontSize: 14
          }}
          onClick={() => router.push('/download/getting-started')}
        >For organizations</Button>
      </div>
    </div>
  );
}
