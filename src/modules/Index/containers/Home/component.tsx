import React, { FC, useEffect } from 'react';
import { Home } from '../../pages/Home';
import { useAtom } from '@reatom/react';
import { activeWorkspaceAtom } from '../../../../store/modules/Organization/atoms';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { useHistory } from 'react-router-dom';
import { loadingAtom } from '../../../../store/ui/RootTemplate/atoms';

export const HomeContainer: FC = () => {
  const [activeWorkspace] = useAtom(activeWorkspaceAtom);
  const [token] = useAtom(authAtom);
  const router = useHistory();
  const [loading] = useAtom(loadingAtom);

  useEffect(() => {
    if (token && activeWorkspace) {
      router.push(`/organization`);
    }
  }, [activeWorkspace, router, token]);

  return !loading ? (
    <Home />
  ) : null;
};
