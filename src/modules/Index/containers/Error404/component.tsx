import React, { FC } from 'react';
import { Error404 } from '../../pages/Error404';


export const Error404Container: FC = () => {

  return (
    <Error404 />
  );
};
