import {
  HomeContainer,
} from './containers';

export const Routes = [
  {
    path: '/',
    component: HomeContainer,
  },
];
