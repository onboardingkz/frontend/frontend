import { UserRoutes } from '../modules/User';
import { IndexRoutes } from '../modules/Index';
import { PremiseRoutes } from '../modules/Premise';
import { OrganizationRoutes } from '../modules/Organization';
import { InstrumentRoutes } from '../modules/Instrument';
import { RulesRoutes } from '../modules/Rule';
import { Error404Container } from '../modules/Index/containers/Error404';
import { ControlPanelRoutes, ControlPanelTemplate } from '../modules/ControlPanel';
import { DepartmentsRoutes } from '../modules/Department';
import { DocumentsRoutes } from '../modules/Document';
import { SignedDocumentsRoutes } from '../modules/SignedDocument';

export const Routes = [
  {
    path: '/',
    routes: IndexRoutes,
    template: null,
    noAuth: true,
  },
  {
    path: '/auth',
    routes: UserRoutes,
    template: null,
  },
  {
    path: '/user',
    routes: UserRoutes,
    template: null,
  },
  {
    path: '/organization',
    routes: OrganizationRoutes,
    template: null,
    fullHeight: true
  },
  {
    path: '/email',
    routes: UserRoutes,
    template: null,
  },
  {
    path: '/download',
    routes: PremiseRoutes,
    template: null,
    noAuth: true
  },
  {
    path: '/instruments',
    routes: InstrumentRoutes,
    template: null
  },
  {
    path: '/departments',
    routes: DepartmentsRoutes,
    template: null
  },
  {
    path: '/rules',
    routes: RulesRoutes,
    template: null
  },
  {
    path: '/documents',
    routes: DocumentsRoutes,
    template: null
  },
  {
    path: '/signed-documents',
    routes: SignedDocumentsRoutes,
    template: null
  },
  {
    path: '/control-panel',
    routes: ControlPanelRoutes,
    template: ControlPanelTemplate,
    fullHeight: true
  },
  {
    path: '*',
    template: null,
    component: Error404Container
  }
];
