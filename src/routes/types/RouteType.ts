import { ComponentType } from 'react';

export type RouteType = {
  readonly path: string;
  readonly component: ComponentType;
  readonly template: ComponentType;
  readonly noAuth?: boolean;
  readonly fullHeight?: boolean;
}
