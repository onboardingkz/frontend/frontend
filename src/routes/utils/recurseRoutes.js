export const recurseRoutes = (routesTree, parentPathNest = '', template = null, noAuth = null, fullHeight = null) => {
  const templateFinal = routesTree.template || template;
  const templateFinalWithFilter = routesTree.hasOwnProperty('template') ? routesTree.template : templateFinal;

  const noAuthFinal = routesTree.noAuth ?? noAuth;
  const fullHeightFinal = routesTree.fullHeight ?? fullHeight;
  const noAuthFinalWithFilter = routesTree.hasOwnProperty('noAuth') ? routesTree.noAuth : noAuthFinal;
  const fullHeightFinalWithFilter = routesTree.hasOwnProperty('fullHeight') ? routesTree.fullHeight : fullHeightFinal;
  return routesTree.routes ?
    [
      ...(routesTree.component ? [{ path: `${parentPathNest}${routesTree.path}`, component: routesTree.component, template: templateFinalWithFilter, noAuth: noAuthFinalWithFilter, fullHeight: fullHeightFinalWithFilter }] : []),
      ...routesTree.routes.map(subRoute => recurseRoutes(subRoute, `${parentPathNest}${routesTree.path}`, templateFinal, noAuthFinal, fullHeightFinal))
    ] :
    {
      path: `${parentPathNest}${routesTree.path}`,
      component: routesTree.component,
      template: templateFinalWithFilter,
      noAuth: noAuthFinalWithFilter,
      fullHeight: fullHeightFinalWithFilter
    }
};
