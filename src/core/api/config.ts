export enum ApiConfig {
  baseUrl = `http://localhost:3000`,
  cdnUrl = `http://localhost:3002`,
  apiRouteGroup = `api`,
}

export const withApiRouteGroup = (endpoint: string) => `${ApiConfig.apiRouteGroup}/${endpoint}`;
