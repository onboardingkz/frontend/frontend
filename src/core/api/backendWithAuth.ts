import axios from 'axios';
import { ApiConfig } from './config';

const workspace = localStorage.getItem('workspaceId')?.split(';');
const token = localStorage.getItem('authToken');

export const backendWithAuth = (organizationId?: string | null, headers: any = null) => axios.create({
  baseURL: ApiConfig.baseUrl,
  headers: {
    ...(token ? { Authorization: `Bearer ${token}` } : {}),
    organizationId: organizationId ?? (workspace?.length === 2 ? workspace[0] : null),
    ...headers
  }
});
