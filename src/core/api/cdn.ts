import axios from 'axios';
import { ApiConfig } from './config';

const token = process.env.REACT_APP_CDN_TOKEN;

export const cdn = axios.create({
  baseURL: ApiConfig.cdnUrl,
  headers: {
    'Content-Type': 'multipart/form-data',
    ...(token ? { Authorization: `Bearer ${token}` } : {}),
  }
});
