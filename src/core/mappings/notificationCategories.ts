export const notificationCategories: Record<string, string> = {
  messages: 'Messages',
  rules: 'Office rules',
  map: 'Office map',
  access: 'Access requests',
  empty: 'Empty'
}
