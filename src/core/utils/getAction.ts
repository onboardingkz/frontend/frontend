export const getAction = (pathname: string) => {
  const pathArr = pathname?.split('/');
  return pathArr?.length > 0 ? pathArr[pathArr.length - 1] : null;
}
