export const isOwner = (organization: any, user: any) => organization?.data?.admin === user?.data?._id;
