export const getMethod = (pathname: string) => {
  const pathArr = pathname?.split('/').filter(n => !!n);
  return pathArr?.length > 0 ? pathArr[0] : null;
}
