import { ApiConfig } from '../api';

export const getAvatar = (avatar: string | null = null) => avatar ? `${ApiConfig.cdnUrl}/${avatar}` : 'https://joeschmoe.io/api/v1/random';
