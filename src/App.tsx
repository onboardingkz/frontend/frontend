import React from 'react';

import {
  BrowserRouter as Router
} from 'react-router-dom';
import { getRouterSwitch } from './routes';
import { createStore } from '@reatom/core';
import { reatomContext } from '@reatom/react';
import { ConfigProvider, message } from 'antd';
import { ErrorBoundary } from './components/ErrorBoundary';
import { io } from 'socket.io-client';
import { SocketConnection } from './components/SocketConnection';

const socket = io(process.env.REACT_APP_SOCKET_URL || 'localhost:4000');


export const App = () => {
  const RouterSwitch = getRouterSwitch();
  const store = createStore();

  message.config({
    top: 10
  });

  return (
    <ErrorBoundary>
      <reatomContext.Provider value={store}>
        <ConfigProvider direction="ltr">
          <SocketConnection.Provider value={socket}>
            <Router>
              {RouterSwitch}
            </Router>
          </SocketConnection.Provider>
        </ConfigProvider>
      </reatomContext.Provider>
    </ErrorBoundary>
  );
}

export default App;
