FROM node:16

WORKDIR /srv/frontend

COPY package.json ./

RUN yarn
COPY . .
RUN yarn build
RUN npm install -g serve

EXPOSE 8080
CMD ["serve", "-s", "build", "-l", "8080"]
